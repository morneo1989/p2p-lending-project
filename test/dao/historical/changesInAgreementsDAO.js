var historicalChangesInAgreementsDao = require("../../../db/dao/historical/changesInAgreementsDAO");
var historicalChangesInAgreements = require("../../../db/model/historical/agreements");
var agreements = require("../../../db/model/main/agreements");

var mongoose = require("mongoose");

var createFullTestHistoricalChangeInAgreement = function(testIndex) {
    var testHistoricalChangeInUser = historicalChangesInAgreements.createHistoricalChangeInAgreement();
    testHistoricalChangeInUser.agreementFk = testIndex.toString();
    testHistoricalChangeInUser.attributesWhichChanged.title = "testPassword" + testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.type = agreements.agreementTypes[0];
    testHistoricalChangeInUser.attributesWhichChanged.stakeholdersFk = ["testStakeHolder" + testIndex];
    return testHistoricalChangeInUser;
};

describe('historical changesInAgreementsDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                historicalChangesInAgreementsDao.init(mongoose);
                historicalChangesInAgreementsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing historical changes in agreements table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for historical changesInAgreementsDAO module");

    describe('#save()', function () {

        it('should save single agreement historical changes', function (done) {
            var expectedHistoricalChangeInAgreement = createFullTestHistoricalChangeInAgreement(0);
            historicalChangesInAgreementsDao.save(expectedHistoricalChangeInAgreement, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving agreement historical changes: " + isError);
                }
                historicalChangesInAgreementsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all agreement historical changes");
                    } else {
                        console.log("Number of saved agreement historical changes: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved agreement historical changes");
                        }
                        var actualHistoricalChangeInAgreement = result[0];
                        if (!expectedHistoricalChangeInAgreement.equals(actualHistoricalChangeInAgreement)) {
                            throw new Error("Invalid content of saved agreement historical changes");
                        }
                    }
                    historicalChangesInAgreementsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all agreement historical changes: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple agreement historical changes', function (done) {
            var expectedHistoricalChangeInAgreement0 = createFullTestHistoricalChangeInAgreement(0);
            var expectedHistoricalChangeInAgreement1 = createFullTestHistoricalChangeInAgreement(1);
            historicalChangesInAgreementsDao.save(expectedHistoricalChangeInAgreement0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving agreement historical change: " + isError);
                }
                historicalChangesInAgreementsDao.save(expectedHistoricalChangeInAgreement1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving agreement historical change: " + isError);
                    }
                    historicalChangesInAgreementsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all agreement historical changes: " + isError);
                        } else {
                            console.log("Number of saved agreement historical changes: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved agreement historical changes");
                            }
                            var actualHistoricalChangesInAgreement0 = result[0];
                            var actualHistoricalChangesInAgreement1 = result[1];
                            if (!expectedHistoricalChangeInAgreement0.equals(actualHistoricalChangesInAgreement0)) {
                                throw new Error("Invalid content of saved agreement historical change");
                            }
                            if (!expectedHistoricalChangeInAgreement1.equals(actualHistoricalChangesInAgreement1)) {
                                throw new Error("Invalid content of saved agreement historical change");
                            }
                        }
                        historicalChangesInAgreementsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all agreement historical changes: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find agreement historical changes by user', function (done) {
            var expectedHistoricalChangesInAgreement = createFullTestHistoricalChangeInAgreement(0);
            historicalChangesInAgreementsDao.save(expectedHistoricalChangesInAgreement, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving agreement historical changes: " + isError);
                }
                historicalChangesInAgreementsDao.findBy({agreementFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding agreement historical changes by user: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding agreement historical changes by user");
                        }
                        var actualHistoricalChangesInAgreement = result[0];
                        if (!expectedHistoricalChangesInAgreement.equals(actualHistoricalChangesInAgreement)) {
                            throw new Error("Invalid content of saved agreement historical changes");
                        }
                    }
                    historicalChangesInAgreementsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all agreement historical changes");
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});