var rejectedLoanApplicationsDao = require("../../../db/dao/historical/rejectedLoanApplicationsDAO");
var rejectedLoanApplications = require("../../../db/model/historical/rejectedLoanApplications");
var auctions = require("../../../db/model/main/auctions");

var mongoose = require("mongoose");

var createFullTestHistoricalRejectedLoanApplication = function(testIndex) {
    var testHistoricalRejectedLoanApplication = rejectedLoanApplications.createRejectedLoanApplication();
    testHistoricalRejectedLoanApplication.borrowerFk = testIndex.toString();
    testHistoricalRejectedLoanApplication.claimedAmount = testIndex;
    testHistoricalRejectedLoanApplication.claimedPayoffPeriodInMonths = testIndex;
    testHistoricalRejectedLoanApplication.claimedRateOfLoan = testIndex;
    testHistoricalRejectedLoanApplication.timestamp = null;
    testHistoricalRejectedLoanApplication.timestampOfRejection = null;
    return testHistoricalRejectedLoanApplication;
};

describe('historical rejectedLoanApplicationsDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                rejectedLoanApplicationsDao.init(mongoose);
                rejectedLoanApplicationsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing historical rejected loan applications table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for historical changesInAuctionsDAO module");

    describe('#save()', function () {

        it('should save single rejected loan applications', function (done) {
            var expectedHistoricalRejectedLoanApplication = createFullTestHistoricalRejectedLoanApplication(0);
            rejectedLoanApplicationsDao.save(expectedHistoricalRejectedLoanApplication, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving rejected loan applications: " + isError);
                }
                rejectedLoanApplicationsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all rejected loan applications");
                    } else {
                        console.log("Number of saved rejected loan applications: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved rejected loan applications");
                        }
                        var actualHistoricalRejectedLoanApplication = result[0];
                        if (!expectedHistoricalRejectedLoanApplication.equals(actualHistoricalRejectedLoanApplication)) {
                            throw new Error("Invalid content of saved rejected loan applications");
                        }
                    }
                    rejectedLoanApplicationsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all rejected loan applications: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple rejected loan applications', function (done) {
            var expectedHistoricalRejectedLoanApplication0 = createFullTestHistoricalRejectedLoanApplication(0);
            var expectedHistoricalRejectedLoanApplication1 = createFullTestHistoricalRejectedLoanApplication(1);
            rejectedLoanApplicationsDao.save(expectedHistoricalRejectedLoanApplication0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving rejected loan application: " + isError);
                }
                rejectedLoanApplicationsDao.save(expectedHistoricalRejectedLoanApplication1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving rejected loan application: " + isError);
                    }
                    rejectedLoanApplicationsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all rejected loan applications: " + isError);
                        } else {
                            console.log("Number of saved auction historical changes: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved rejected loan applications");
                            }
                            var actualHistoricalRejectedLoanApplication0 = result[0];
                            var actualHistoricalRejectedLoanApplication1 = result[1];
                            if (!expectedHistoricalRejectedLoanApplication0.equals(actualHistoricalRejectedLoanApplication0)) {
                                throw new Error("Invalid content of saved rejected loan application");
                            }
                            if (!expectedHistoricalRejectedLoanApplication1.equals(actualHistoricalRejectedLoanApplication1)) {
                                throw new Error("Invalid content of saved rejected loan application");
                            }
                        }
                        rejectedLoanApplicationsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all rejected loan applications: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find rejected loan applications by borrower', function (done) {
            var expectedHistoricalRejectedLoanApplication = createFullTestHistoricalRejectedLoanApplication(0);
            rejectedLoanApplicationsDao.save(expectedHistoricalRejectedLoanApplication, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving rejected loan applications: " + isError);
                }
                rejectedLoanApplicationsDao.findBy({borrowerFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding rejected loan applications by borrower: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding rejected loan applications by borrower");
                        }
                        var actualHistoricalRejectedLoanApplication = result[0];
                        if (!expectedHistoricalRejectedLoanApplication.equals(actualHistoricalRejectedLoanApplication)) {
                            throw new Error("Invalid content of saved auction historical changes");
                        }
                    }
                    rejectedLoanApplicationsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all rejected loan applications");
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});