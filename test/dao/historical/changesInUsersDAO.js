var historicalChangesInUsersDao = require("../../../db/dao/historical/changesInUsersDAO");
var historicalChangesInUsers = require("../../../db/model/historical/users");
var users = require("../../../db/model/main/users");

var mongoose = require("mongoose");

var createFullTestHistoricalChangeInUser = function(testIndex) {
    var testHistoricalChangeInUser = historicalChangesInUsers.createHistoricalChangeInUser();
    testHistoricalChangeInUser.userFk = testIndex.toString();
    testHistoricalChangeInUser.attributesWhichChanged.login = "testPassword" + testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.role = users.userRoles.ADMIN;
    testHistoricalChangeInUser.attributesWhichChanged.name = "testName" + testIndex;
    return testHistoricalChangeInUser;
};

describe('historical changesInUsersDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                historicalChangesInUsersDao.init(mongoose);
                historicalChangesInUsersDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing historical changes in users table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for historicalChangesInUsersDAO module");

    describe('#save()', function () {

        it('should save single user historical changes', function (done) {
            var expectedHistoricalChangeInUser = createFullTestHistoricalChangeInUser(0);
            historicalChangesInUsersDao.save(expectedHistoricalChangeInUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user historical changes: " + isError);
                }
                historicalChangesInUsersDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all user historical changes");
                    } else {
                        console.log("Number of saved user historical changes: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved user historical changes");
                        }
                        var actualHistoricalChangeInUser = result[0];
                        if (!expectedHistoricalChangeInUser.equals(actualHistoricalChangeInUser)) {
                            throw new Error("Invalid content of saved user historical changes");
                        }
                    }
                    historicalChangesInUsersDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user historical changes: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple user historical changes', function (done) {
            var expectedHistoricalChangeInUser0 = createFullTestHistoricalChangeInUser(0);
            var expectedHistoricalChangeInUser1 = createFullTestHistoricalChangeInUser(1);
            historicalChangesInUsersDao.save(expectedHistoricalChangeInUser0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user historical change: " + isError);
                }
                historicalChangesInUsersDao.save(expectedHistoricalChangeInUser1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving user historical change: " + isError);
                    }
                    historicalChangesInUsersDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all user historical changes: " + isError);
                        } else {
                            console.log("Number of saved user historical changes: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved user historical changes");
                            }
                            var actualHistoricalChangesInUser0 = result[0];
                            var actualHistoricalChangesInUser1 = result[1];
                            if (!expectedHistoricalChangeInUser0.equals(actualHistoricalChangesInUser0)) {
                                throw new Error("Invalid content of saved user historical change");
                            }
                            if (!expectedHistoricalChangeInUser1.equals(actualHistoricalChangesInUser1)) {
                                throw new Error("Invalid content of saved user historical change");
                            }
                        }
                        historicalChangesInUsersDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all user historical changes: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find user historical changes by user', function (done) {
            var expectedHistoricalChangesInUser = createFullTestHistoricalChangeInUser(0);
            historicalChangesInUsersDao.save(expectedHistoricalChangesInUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user historical changes: " + isError);
                }
                historicalChangesInUsersDao.findBy({userFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding user historical changes by user: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding user historical changes by user");
                        }
                        var actualHistoricalChangesInUser = result[0];
                        if (!expectedHistoricalChangesInUser.equals(actualHistoricalChangesInUser)) {
                            throw new Error("Invalid content of saved user historical changes");
                        }
                    }
                    historicalChangesInUsersDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user historical changes");
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});