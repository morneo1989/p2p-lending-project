var historicalChangesInAuctionsDao = require("../../../db/dao/historical/changesInAuctionsDAO");
var historicalChangesInAuctions = require("../../../db/model/historical/auctions");
var auctions = require("../../../db/model/main/auctions");

var mongoose = require("mongoose");

var createFullTestHistoricalChangeInAuctions = function(testIndex) {
    var testHistoricalChangeInUser = historicalChangesInAuctions.createHistoricalChangeInAuction();
    testHistoricalChangeInUser.auctionFk = testIndex.toString();
    testHistoricalChangeInUser.timestampOfAttributeChanges = null;
    testHistoricalChangeInUser.attributesWhichChanged.loanTarget = auctions.loanTargets[0];
    testHistoricalChangeInUser.attributesWhichChanged.loanAmount = testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.rateOfLoan = testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.timestampOfCreation = null;
    testHistoricalChangeInUser.attributesWhichChanged.timestampOfRejection = null;
    testHistoricalChangeInUser.attributesWhichChanged.timestampOfStart = null;
    testHistoricalChangeInUser.attributesWhichChanged.timestampOfFinish = null;
    testHistoricalChangeInUser.attributesWhichChanged.borrowerFk = testIndex.toString();
    testHistoricalChangeInUser.attributesWhichChanged.borrowerApplication.claimedAmount = testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.borrowerApplication.claimedPayoffPeriodInMonths = testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.borrowerApplication.claimedRateOfLoan = testIndex;
    testHistoricalChangeInUser.attributesWhichChanged.borrowerApplication.timestamp = null;
    return testHistoricalChangeInUser;
};

describe('historical changesInAuctionsDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                historicalChangesInAuctionsDao.init(mongoose);
                historicalChangesInAuctionsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing historical changes in auctions table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for historical changesInAuctionsDAO module");

    describe('#save()', function () {

        it('should save single auction historical changes', function (done) {
            var expectedHistoricalChangeInAuction = createFullTestHistoricalChangeInAuctions(0);
            historicalChangesInAuctionsDao.save(expectedHistoricalChangeInAuction, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction historical changes: " + isError);
                }
                historicalChangesInAuctionsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all auction historical changes");
                    } else {
                        console.log("Number of saved auction historical changes: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved auction historical changes");
                        }
                        var actualHistoricalChangeInAuction = result[0];
                        if (!expectedHistoricalChangeInAuction.equals(actualHistoricalChangeInAuction)) {
                            throw new Error("Invalid content of saved auction historical changes");
                        }
                    }
                    historicalChangesInAuctionsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all auction historical changes: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple auction historical changes', function (done) {
            var expectedHistoricalChangeInAuction0 = createFullTestHistoricalChangeInAuctions(0);
            var expectedHistoricalChangeInAuction1 = createFullTestHistoricalChangeInAuctions(1);
            historicalChangesInAuctionsDao.save(expectedHistoricalChangeInAuction0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction historical change: " + isError);
                }
                historicalChangesInAuctionsDao.save(expectedHistoricalChangeInAuction1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving auction historical change: " + isError);
                    }
                    historicalChangesInAuctionsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all auction historical changes: " + isError);
                        } else {
                            console.log("Number of saved auction historical changes: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved auction historical changes");
                            }
                            var actualHistoricalChangesInAuction0 = result[0];
                            var actualHistoricalChangesInAuction1 = result[1];
                            if (!expectedHistoricalChangeInAuction0.equals(actualHistoricalChangesInAuction0)) {
                                throw new Error("Invalid content of saved auction historical change");
                            }
                            if (!expectedHistoricalChangeInAuction1.equals(actualHistoricalChangesInAuction1)) {
                                throw new Error("Invalid content of saved auction historical change");
                            }
                        }
                        historicalChangesInAuctionsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all auction historical changes: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find auction historical changes by user', function (done) {
            var expectedHistoricalChangesInAuction = createFullTestHistoricalChangeInAuctions(0);
            historicalChangesInAuctionsDao.save(expectedHistoricalChangesInAuction, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction historical changes: " + isError);
                }
                historicalChangesInAuctionsDao.findBy({auctionFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding auction historical changes by user: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding auction historical changes by user");
                        }
                        var actualHistoricalChangesInAuction = result[0];
                        if (!expectedHistoricalChangesInAuction.equals(actualHistoricalChangesInAuction)) {
                            throw new Error("Invalid content of saved auction historical changes");
                        }
                    }
                    historicalChangesInAuctionsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all auction historical changes");
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});