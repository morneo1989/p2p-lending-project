var historicalChangesInInvestorSharesDao = require("../../../db/dao/historical/changesInInvestorSharesDAO");
var historicalChangesInInvestorShares = require("../../../db/model/historical/investorShares");

var mongoose = require("mongoose");

var createFullTestHistoricalChangeInInvestorShares = function(testIndex) {
    var testHistoricalChangeInInvestorShare = historicalChangesInInvestorShares.createHistoricalChangeInInvestorShare();
    testHistoricalChangeInInvestorShare.investorShareFk = testIndex.toString();
    testHistoricalChangeInInvestorShare.timestampOfAttributeChanges = null;
    testHistoricalChangeInInvestorShare.attributesWhichChanged.amountOfInvestment = testIndex;
    testHistoricalChangeInInvestorShare.attributesWhichChanged.investmentConfirmed = false;
    testHistoricalChangeInInvestorShare.attributesWhichChanged.investorFk = testIndex.toString();
    testHistoricalChangeInInvestorShare.attributesWhichChanged.auctionFk = testIndex.toString();
    testHistoricalChangeInInvestorShare.attributesWhichChanged.investmentAtAfterMarket = false;
    testHistoricalChangeInInvestorShare.attributesWhichChanged.timestamp = null;
    return testHistoricalChangeInInvestorShare;
};

describe('historical changesInInvestorSharesDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                historicalChangesInInvestorSharesDao.init(mongoose);
                historicalChangesInInvestorSharesDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing historical changes in auctions table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for historical changesInAuctionsDAO module");

    describe('#save()', function () {

        it('should save single investor share historical changes', function (done) {
            var expectedHistoricalChangeInInvestorShare = createFullTestHistoricalChangeInInvestorShares(0);
            historicalChangesInInvestorSharesDao.save(expectedHistoricalChangeInInvestorShare, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction historical changes: " + isError);
                }
                historicalChangesInInvestorSharesDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all auction historical changes");
                    } else {
                        console.log("Number of saved auction historical changes: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved auction historical changes");
                        }
                        var actualHistoricalChangeInInvestorShare = result[0];
                        if (!expectedHistoricalChangeInInvestorShare.equals(actualHistoricalChangeInInvestorShare)) {
                            throw new Error("Invalid content of saved auction historical changes");
                        }
                    }
                    historicalChangesInInvestorSharesDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all auction historical changes: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple investor share historical changes', function (done) {
            var expectedHistoricalChangeInInvestorShare0 = createFullTestHistoricalChangeInInvestorShares(0);
            var expectedHistoricalChangeInInvestorShare1 = createFullTestHistoricalChangeInInvestorShares(1);
            historicalChangesInInvestorSharesDao.save(expectedHistoricalChangeInInvestorShare0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving investor share historical change: " + isError);
                }
                historicalChangesInInvestorSharesDao.save(expectedHistoricalChangeInInvestorShare1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving investor share historical change: " + isError);
                    }
                    historicalChangesInInvestorSharesDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all investor share historical changes: " + isError);
                        } else {
                            console.log("Number of saved investor share historical changes: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved investor share historical changes");
                            }
                            var actualHistoricalChangesInInvestorShare0 = result[0];
                            var actualHistoricalChangesInInvestorShare1 = result[1];
                            if (!expectedHistoricalChangeInInvestorShare0.equals(actualHistoricalChangesInInvestorShare0)) {
                                throw new Error("Invalid content of saved investor share historical change");
                            }
                            if (!expectedHistoricalChangeInInvestorShare1.equals(actualHistoricalChangesInInvestorShare1)) {
                                throw new Error("Invalid content of saved investor share historical change");
                            }
                        }
                        historicalChangesInInvestorSharesDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all investor share historical changes: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find investor share historical changes by investor', function (done) {
            var expectedHistoricalChangesInAuction = createFullTestHistoricalChangeInInvestorShares(0);
            historicalChangesInInvestorSharesDao.save(expectedHistoricalChangesInAuction, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving investor share historical changes: " + isError);
                }
                historicalChangesInInvestorSharesDao.findBy({investorShareFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding investor share historical changes by investor: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding investor share historical changes by investor");
                        }
                        var actualHistoricalChangesInAuction = result[0];
                        if (!expectedHistoricalChangesInAuction.equals(actualHistoricalChangesInAuction)) {
                            throw new Error("Invalid content of saved investor share historical changes");
                        }
                    }
                    historicalChangesInInvestorSharesDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all investor share historical changes");
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});