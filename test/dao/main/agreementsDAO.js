var agreementsDao = require("../../../db/dao/main/agreementsDAO");
var agreements = require("../../../db/model/main/agreements");

var mongoose = require("mongoose");

var createFullTestAgreement = function(testIndex) {
    var testAgreement = agreements.createAgreement();
    testAgreement.title = "testTitle" + testIndex;
    testAgreement.document = null;
    testAgreement.type = agreements.agreementTypes[0];
    testAgreement.stakeholdersFk = ["stakeHolderFirst" + testIndex, "stakeHolderSecond" + testIndex];
    testAgreement.timestampOfCreation = null;
    return testAgreement;
};

describe('agreementsDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                agreementsDao.init(mongoose);
                agreementsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing agreements table before unit tests: " + isError);
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for agreementsDAO module");

    describe('#save()', function () {
        it('should save single agreement', function (done) {
            var expectedAgreement = createFullTestAgreement(0);
            agreementsDao.save(expectedAgreement, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving agreement: " + isError);
                }
                agreementsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all agreements: " + isError);
                    } else {
                        console.log("Number of saved agreements: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved agreements");
                        }
                        var actualAgreement = result[0];
                        if (!expectedAgreement.equals(actualAgreement)) {
                            throw new Error("Invalid content of saved agreement");
                        }
                    }
                    agreementsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all agreements");
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple agreements', function (done) {
            var expectedAgreement0 = createFullTestAgreement(0);
            var expectedAgreement1 = createFullTestAgreement(1);
            agreementsDao.save(expectedAgreement0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving agreement: " + isError);
                }
                agreementsDao.save(expectedAgreement1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving agreement: " + isError);
                    }
                    agreementsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all agreements: " + isError);
                        } else {
                            console.log("Number of saved agreements: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved agreements");
                            }
                            var actualAgreement0 = result[0];
                            var actualAgreement1 = result[1];
                            if (!expectedAgreement0.equals(actualAgreement0)) {
                                throw new Error("Invalid content of saved agreement");
                            }
                            if (!expectedAgreement1.equals(actualAgreement1)) {
                                throw new Error("Invalid content of saved agreement");
                            }
                        }
                        agreementsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all agreements: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find agreement by user', function (done) {
            var expectedAgreement = createFullTestAgreement(0);
            agreementsDao.save(expectedAgreement, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving agreement: " + isError);
                }
                agreementsDao.findBy({stakeholdersFk: "stakeHolderFirst0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding agreement by user: " + isError);
                    } else {
                        console.log("Number of saved agreements: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved agreements");
                        }
                        var actualAgreement = result[0];
                        if (!expectedAgreement.equals(actualAgreement)) {
                            throw new Error("Invalid content of saved agreement");
                        }
                    }
                    agreementsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all agreements: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});