var investorSharesDao = require("../../../db/dao/main/investorSharesDAO");
var investorShares = require("../../../db/model/main/investorShares");

var mongoose = require("mongoose");

var createFullTestInvestorShares = function(testIndex) {
    var testInvestorShares = investorShares.createInvestorShare();
    testInvestorShares.amountOfInvestment = testIndex;
    testInvestorShares.investmentConfirmed = false;
    testInvestorShares.auctionFk = testIndex.toString();
    testInvestorShares.investorFk = testIndex.toString();
    testInvestorShares.investmentAtAfterMarket = true;
    testInvestorShares.timestamp = null;
    return testInvestorShares;
};

describe('investorSharesDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                investorSharesDao.init(mongoose);
                investorSharesDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing investor shares table before unit tests: " + isError);
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for investorSharesDAO module");

    describe('#save()', function () {
        it('should save single investor share', function (done) {
            var expectedInvestorShare = createFullTestInvestorShares(0);
            investorSharesDao.save(expectedInvestorShare, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving investor share: " + isError);
                }
                investorSharesDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all investor shares: " + isError);
                    } else {
                        console.log("Number of saved historicalChangesInAgreements: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved investor shares");
                        }
                        var actualInvestorShare = result[0];
                        if (!expectedInvestorShare.equals(actualInvestorShare)) {
                            throw new Error("Invalid content of saved investor share");
                        }
                    }
                    investorSharesDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all investor shares");
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple investor shares', function (done) {
            var expectedInvestorShare0 = createFullTestInvestorShares(0);
            var expectedInvestorShare1 = createFullTestInvestorShares(1);
            investorSharesDao.save(expectedInvestorShare0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving investor share: " + isError);
                }
                investorSharesDao.save(expectedInvestorShare1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving investor share: " + isError);
                    }
                    investorSharesDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all investor shares: " + isError);
                        } else {
                            console.log("Number of saved historicalChangesInAgreements: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved investor shares");
                            }
                            var actualInvestorShare0 = result[0];
                            var actualInvestorShare1 = result[1];
                            if (!expectedInvestorShare0.equals(actualInvestorShare0)) {
                                throw new Error("Invalid content of saved investor share");
                            }
                            if (!expectedInvestorShare1.equals(actualInvestorShare1)) {
                                throw new Error("Invalid content of saved investor share");
                            }
                        }
                        investorSharesDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all investor shares: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find investor share by investor', function (done) {
            var expectedInvestorShare = createFullTestInvestorShares(0);
            investorSharesDao.save(expectedInvestorShare, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving investor share: " + isError);
                }
                investorSharesDao.findBy({investorFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding investor share by investor: " + isError);
                    } else {
                        console.log("Number of saved investor shares: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved investor shares");
                        }
                        var actualInvestorShare = result[0];
                        if (!expectedInvestorShare.equals(actualInvestorShare)) {
                            throw new Error("Invalid content of saved investor share");
                        }
                    }
                    investorSharesDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all investor shares: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    describe('#updateBy()', function () {
        it('should update investor share by investor', function (done) {
            var expectedInvestorShare = createFullTestInvestorShares(0);
            investorSharesDao.save(expectedInvestorShare, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving investor share: " + isError);
                }
                investorSharesDao.updateBy({investorFk: "0"}, {investmentConfirmed: true}, function (isError) {
                    if (isError) {
                        throw new Error("Problem with finding investor share by investor: " + isError);
                    } else {
                        investorSharesDao.findBy({investorFk: "0"}, function (isError, result) {
                            if (isError) {
                                throw new Error("Problem with finding investor share by investor: " + isError);
                            } else {
                                console.log("Number of saved investor shares: " + result.length + "  ");
                                if(result.length != 1) {
                                    throw new Error("Invalid number of retrieved investor shares");
                                }
                                var actualInvestorShare = result[0];
                                if (actualInvestorShare.investmentConfirmed !== true) {
                                    throw new Error("Invalid content of saved investor share");
                                }
                            }
                            investorSharesDao.removeAll(function (isError, numberOfRemoved) {
                                if (isError) {
                                    throw new Error("Problem with removing all investor shares: " + isError);
                                }
                                done();
                            });
                        });
                    }
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});