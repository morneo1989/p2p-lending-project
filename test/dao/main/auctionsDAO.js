var auctionsDao = require("../../../db/dao/main/auctionsDAO");
var auctions = require("../../../db/model/main/auctions");

var mongoose = require("mongoose");

var createFullTestAuction = function(testIndex) {
    var testAuction = auctions.createAuction();
    testAuction.borrowerFk = testIndex;
    testAuction.loanAmount = testIndex;
    testAuction.loanTarget = auctions.loanTargets[0];
    testAuction.timestampOfCreation = null;
    testAuction.timestampOfFinish = null;
    testAuction.timestampOfStart = null;
    testAuction.timestampOfRejection = null;
    testAuction.borrowerFk = testIndex.toString();
    testAuction.borrowerApplication.claimedAmount = testIndex;
    testAuction.borrowerApplication.claimedPayoffPeriodInMonths = testIndex;
    testAuction.borrowerApplication.claimedRateOfLoan = testIndex;
    testAuction.borrowerApplication.timestamp = null;
    testAuction.payOffSchedule.paidLoanAmount = testIndex;
    testAuction.payOffSchedule.installments = [{installmentAmount: testIndex, dateOfRepayment: null}, {installmentAmount: testIndex + 1, dateOfRepayment: null}];
    return testAuction;
};

describe('auctionsDao', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                auctionsDao.init(mongoose);
                auctionsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing auctions table before unit tests: " + isError);
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for auctionsDAO module");

    describe('#save()', function () {
        it('should save single auction', function (done) {
            var expectedAuction = createFullTestAuction(0);
            auctionsDao.save(expectedAuction, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction: " + isError);
                }
                auctionsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all auctions: " + isError);
                    } else {
                        console.log("Number of saved auctions: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved auctions");
                        }
                        var actualAuction = result[0];
                        if (!expectedAuction.equals(actualAuction)) {
                            throw new Error("Invalid content of saved auction");
                        }
                    }
                    auctionsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all auctions");
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple auctions', function (done) {
            var expectedAuction0 = createFullTestAuction(0);
            var expectedAuction1 = createFullTestAuction(1);
            auctionsDao.save(expectedAuction0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction: " + isError);
                }
                auctionsDao.save(expectedAuction1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving auction: " + isError);
                    }
                    auctionsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all auctions: " + isError);
                        } else {
                            console.log("Number of saved auctions: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved auctions");
                            }
                            var actualAuction0 = result[0];
                            var actualAuction1 = result[1];
                            if (!expectedAuction0.equals(actualAuction0)) {
                                throw new Error("Invalid content of saved auction");
                            }
                            if (!expectedAuction1.equals(actualAuction1)) {
                                throw new Error("Invalid content of saved auction");
                            }
                        }
                        auctionsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all auctions: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find auction by borrower', function (done) {
            var expectedAuction = createFullTestAuction(0);
            auctionsDao.save(expectedAuction, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving auction: " + isError);
                }
                auctionsDao.findBy({borrowerFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding auction by borrower: " + isError);
                    } else {
                        console.log("Number of saved auctions: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved auctions");
                        }
                        var actualAuction = result[0];
                        if (!expectedAuction.equals(actualAuction)) {
                            throw new Error("Invalid content of saved auction");
                        }
                    }
                    auctionsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all auctions: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});