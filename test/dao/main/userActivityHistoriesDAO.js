var userActivityHistoriesDao = require("../../../db/dao/main/userActivityHistoriesDAO");
var userActivityHistories = require("../../../db/model/main/userActivityHistories");

var mongoose = require("mongoose");

var createFullTestUserActivity = function(testIndex) {
    var testUserActivity = userActivityHistories.createActivityHistory();
    testUserActivity.activityType = userActivityHistories.userActivityTypes.BORROWER_REGISTRATION_ATTEMPT;
    testUserActivity.userFk = "user" + testIndex;
    testUserActivity.timestamp = null;
    testUserActivity.activityParameters = [{nameOfParameter: "someParam", value: 1}];
    return testUserActivity;
};

describe('userActivityHistoriesDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                userActivityHistoriesDao.init(mongoose);
                userActivityHistoriesDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing user activity history table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for userActivityHistoriesDAO module");

    describe('#save()', function () {

        it('should save single user activity', function (done) {
            var expectedUserActivity = createFullTestUserActivity(0);
            userActivityHistoriesDao.save(expectedUserActivity, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user activity: " + isError);
                }
                userActivityHistoriesDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all user activities: " + isError);
                    } else {
                        console.log("Number of saved user activities: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved user activities");
                        }
                        var actualUserActivity = result[0];
                        if (!expectedUserActivity.equals(actualUserActivity)) {
                            throw new Error("Invalid content of saved user activity");
                        }
                    }
                    userActivityHistoriesDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user activities: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple user comments', function (done) {
            var expectedUserActivity0 = createFullTestUserActivity(0);
            var expectedUserActivity1 = createFullTestUserActivity(1);
            userActivityHistoriesDao.save(expectedUserActivity0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user activity: " + isError);
                }
                userActivityHistoriesDao.save(expectedUserActivity1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving user activity: " + isError);
                    }
                    userActivityHistoriesDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all user activities: " + isError);
                        } else {
                            console.log("Number of saved user activities: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved user activities");
                            }
                            var actualUserActivity0 = result[0];
                            var actualUserActivity1 = result[1];
                            if (!expectedUserActivity0.equals(actualUserActivity0)) {
                                throw new Error("Invalid content of saved user activity");
                            }
                            if (!expectedUserActivity1.equals(actualUserActivity1)) {
                                throw new Error("Invalid content of saved user activity");
                            }
                        }
                        userActivityHistoriesDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all user activities: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find user activity by user', function (done) {
            var expectedUserActivity = createFullTestUserActivity(0);
            userActivityHistoriesDao.save(expectedUserActivity, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user activity: " + isError);
                }
                userActivityHistoriesDao.findBy({userFk: "user0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding user activity by user: " + isError);
                    } else {
                        console.log("Number of saved user comments: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved user activities");
                        }
                        var actualUserActivity = result[0];
                        if (!expectedUserActivity.equals(actualUserActivity)) {
                            throw new Error("Invalid content of saved user activity");
                        }
                    }
                    userActivityHistoriesDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user activities");
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});