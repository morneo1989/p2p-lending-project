var usersCommentsDao = require("../../../db/dao/main/userCommentsDAO");
var userComments = require("../../../db/model/main/userComments");

var mongoose = require("mongoose");

var createFullTestUserComment = function(testIndex) {
    var testUserComment = userComments.createUserComment();
    testUserComment.comment = "testComment" + testIndex;
    testUserComment.commentModerated = false;
    testUserComment.platformUsageRating = testIndex;
    testUserComment.userFk = testIndex.toString();
    return testUserComment;
};

describe('userCommentsDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                usersCommentsDao.init(mongoose);
                usersCommentsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing user comments table before unit tests: " + isError);
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for userCommentsDAO module");

    describe('#save()', function () {

        it('should save single user comment', function (done) {
            var expectedUserComment = createFullTestUserComment(0);
            usersCommentsDao.save(expectedUserComment, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user comment: " + isError);
                }
                usersCommentsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all user comments: " + isError);
                    } else {
                        console.log("Number of saved user comments: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved user comments");
                        }
                        var actualUserComment = result[0];
                        if (!expectedUserComment.equals(actualUserComment)) {
                            throw new Error("Invalid content of saved user comment");
                        }
                    }
                    usersCommentsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user comments: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple user comments', function (done) {
            var expectedUserComment0 = createFullTestUserComment(0);
            var expectedUserComment1 = createFullTestUserComment(1);
            usersCommentsDao.save(expectedUserComment0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user comment: " + isError);
                }
                usersCommentsDao.save(expectedUserComment1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving user comment: " + isError);
                    }
                    usersCommentsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all user comments: " + isError);
                        } else {
                            console.log("Number of saved user comments: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved user comments");
                            }
                            var actualUserComment0 = result[0];
                            var actualUserComment1 = result[1];
                            if (!expectedUserComment0.equals(actualUserComment0)) {
                                throw new Error("Invalid content of saved user comment");
                            }
                            if (!expectedUserComment1.equals(actualUserComment1)) {
                                throw new Error("Invalid content of saved user comment");
                            }
                        }
                        usersCommentsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all user comments: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find user comment by user', function (done) {
            var expectedUserComment = createFullTestUserComment(0);
            usersCommentsDao.save(expectedUserComment, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user comment: " + isError);
                }
                usersCommentsDao.findBy({userFk: "0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding user comment by user: " + isError);
                    } else {
                        console.log("Number of saved user comments: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved user comments");
                        }
                        var actualUserComment = result[0];
                        if (!expectedUserComment.equals(actualUserComment)) {
                            throw new Error("Invalid content of saved user comment: " + isError);
                        }
                    }
                    usersCommentsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user comments: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});