var usersDao = require("../../../db/dao/main/usersDAO");
var users = require("../../../db/model/main/users");

var mongoose = require("mongoose");

var createFullTestUser = function(testIndex) {
    var testUser = users.createUser();
    testUser.email = "testEmail" + testIndex;
    testUser.password = "testPassword" + testIndex;
    testUser.role = users.userRoles.ADMIN;
    testUser.name = "testName" + testIndex;
    testUser.surname = "testSurname" + testIndex;
    testUser.category.name = "testCategory" + testIndex;
    testUser.category.loanAmountCoefficient = testIndex;
    testUser.category.payoffPeriodCoefficient = testIndex;
    testUser.ratingparameters.age = testIndex;
    testUser.ratingparameters.employmentArea = users.employmentAreas[1];
    testUser.ratingparameters.maritialStatus = users.maritalStatuses[0];
    testUser.ratingparameters.maritialStatusPeriodInMonths = testIndex;
    testUser.ratingparameters.numberOfNonPaidOffLoans = testIndex;
    testUser.ratingparameters.numberOfPaidOffLoans = testIndex;
    testUser.autobid.interestMinRateOfLoan = testIndex;
    testUser.autobid.interestMaxRateOfLoan = testIndex;
    testUser.autobid.loanMinAmount = testIndex;
    testUser.autobid.loanMaxAmount = testIndex;
    testUser.autobid.loanMaxRepaymentPeriodInMonths = testIndex;
    testUser.bankAccount.number = testIndex.toString();
    testUser.bankAccount.currentBalance = testIndex;
    testUser.bankAccount.currentIncome = testIndex;
    testUser.bankAccount.pendingBankTransfers = [{
        transferDetails: {
            title: testIndex.toString()
        },
        cashAmount: testIndex,
        operationType: users.bankAccount.operationType.INCOMING,
        timestampOfCreation: null
    }];
    testUser.bankAccount.confirmedBankTransfers = [{
        transferDetails: {
            title: testIndex.toString()
        },
        cashAmount: testIndex,
        operationType: users.bankAccount.operationType.OUTGOING,
        timestampOfCreation: null
    }];
    return testUser;
};

describe('usersDAO', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                usersDao.init(mongoose);
                usersDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing users table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for usersDAO module");

    describe('#save()', function () {

        it('should save single user', function (done) {
            var expectedUser = createFullTestUser(0);
            usersDao.save(expectedUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user: " + isError);
                }
                usersDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all user: " + isError);
                    } else {
                        console.log("Number of saved users: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved users");
                        }
                        var actualUser = result[0];
                        if (!expectedUser.equals(actualUser)) {
                            throw new Error("Invalid content of saved users");
                        }
                    }
                    usersDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple users', function (done) {
            var expectedUser0 = createFullTestUser(0);
            var expectedUser1 = createFullTestUser(1);
            usersDao.save(expectedUser0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user: " + isError);
                }
                usersDao.save(expectedUser1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving user: " + isError);
                    }
                    usersDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all user: " + isError);
                        } else {
                            console.log("Number of saved users: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved users");
                            }
                            var actualUser0 = result[0];
                            var actualUser1 = result[1];
                            if (!expectedUser0.equals(actualUser0)) {
                                throw new Error("Invalid content of saved user");
                            }
                            if (!expectedUser1.equals(actualUser1)) {
                                throw new Error("Invalid content of saved user");
                            }
                        }
                        usersDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all user: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findBy()', function () {
        it('should find user by surname', function (done) {
            var expectedUser = createFullTestUser(0);
            usersDao.save(expectedUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user: " + isError);
                }
                usersDao.findBy({surname: "testSurname0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding user by surname: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding user by surname: " + isError);
                        }
                        var actualUser = result[0];
                        if (!expectedUser.equals(actualUser)) {
                            throw new Error("Invalid content of saved users");
                        }
                    }
                    usersDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    describe('#findSelectedBy()', function () {
        it('should find user name and surname by surname', function (done) {
            var expectedUser = createFullTestUser(0);
            usersDao.save(expectedUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user: " + isError);
                }
                usersDao.findSelectedBy("name surname", {surname: "testSurname0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding user by surname: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding user name and surname by surname: " + isError);
                        }
                        var actualUser = result[0];
                        if (expectedUser.name !== actualUser.name || expectedUser.surname !== actualUser.surname || actualUser.role != undefined) {
                            throw new Error("Invalid content of saved users");
                        }
                    }
                    usersDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    describe('#findSelectedBy()', function () {
        it('should find user name, surname and pending bank account operations by surname', function (done) {
            var expectedUser = createFullTestUser(0);
            usersDao.save(expectedUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user: " + isError);
                }
                usersDao.findSelectedBy("name surname bankAccount.pendingBankTransfers", {surname: "testSurname0"}, function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding user by surname: " + isError);
                    } else {
                        if(result.length != 1) {
                            throw new Error("Problem with finding user name and surname by surname: " + isError);
                        }
                        var actualUser = result[0];
                        if (expectedUser.name !== actualUser.name || expectedUser.surname !== actualUser.surname || actualUser.role != undefined
                        || actualUser.bankAccount.confirmedBankTransfers != undefined || actualUser.bankAccount.pendingBankTransfers == undefined) {
                            throw new Error("Invalid content of saved users");
                        }
                    }
                    usersDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all user: " + isError);
                        }
                        done();
                    });
                });
            });
        });
    });

    describe('#updateBy()', function () {
        it('should update user by name', function (done) {
            var expectedUser = createFullTestUser(0);
            usersDao.save(expectedUser, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving user: " + isError);
                }
                usersDao.updateBy({name: "testName0"}, {category: {name: "testCategory0", loanAmountCoefficient: 45.45, payoffPeriodCoefficient: 0}}, function (isError) {
                    if (isError) {
                        throw new Error("Problem with updating user by name: " + isError);
                    }
                    usersDao.findBy({name: "testName0"}, function(isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding user by name: " + isError);
                        }
                        if(result.length != 1) {
                            throw new Error("Problem with finding user by name: " + isError);
                        }
                        var actualUser = result[0];
                        if (!expectedUser.equals(actualUser)) {
                            throw new Error("Invalid content of saved users");
                        }
                        usersDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all user: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});