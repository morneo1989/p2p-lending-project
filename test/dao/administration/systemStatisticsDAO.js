var systemStatisticsDao = require("../../../db/dao/administration/systemStatisticsDAO");
var systemStatistics = require("../../../db/model/administration/systemStatistics");
var agreements = require("../../../db/model/main/agreements");
var moment = require("moment");

var mongoose = require("mongoose");

var createFullTestSystemStatistic = function(testIndex) {
    var testSystemStatistic = systemStatistics.createSystemStatistic();
    testSystemStatistic.cashFlowAmount = testIndex;
    testSystemStatistic.notPaidAuctionsAmount = testIndex;
    testSystemStatistic.paidAuctionsAmount = testIndex;
    testSystemStatistic.openedAuctionsAmount = testIndex;
    testSystemStatistic.usersAmount = testIndex;
    testSystemStatistic.rejectedLoanApplicationsAmount = testIndex;
    testSystemStatistic.timestampOfStatisticsSnapshot = moment().add(testIndex, 'days').toDate();
    return testSystemStatistic;
};

describe('systemStatisticsDao', function () {

    before(function(done) {
        mongoose.connect('mongodb://localhost:27017/p2pLendingDB_test', function (err) {
            if (err) {
                throw new Error("No mongo DB instance under following url: mongodb://localhost:27017/p2pLendingDB_test")
            } else {
                systemStatisticsDao.init(mongoose);
                systemStatisticsDao.removeAll(function(isError) {
                    if(isError) {
                        throw new Error("Problem with clearing system statistics in agreements table before unit tests");
                    }
                    done();
                });
            }
        });
    });

    console.log("Start unit tests for historical changesInAgreementsDAO module");

    describe('#save()', function () {

        it('should save single system statistic', function (done) {
            var expectedSystemStatistic = createFullTestSystemStatistic(0);
            systemStatisticsDao.save(expectedSystemStatistic, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving system statistic: " + isError);
                }
                systemStatisticsDao.findAll(function (isError, result) {
                    if (isError) {
                        throw new Error("Problem with finding all system statistics");
                    } else {
                        console.log("Number of saved system statistics: " + result.length + "  ");
                        if(result.length != 1) {
                            throw new Error("Invalid number of retrieved system statistics");
                        }
                        var actualSystemStatistic = result[0];
                        if (!expectedSystemStatistic.equals(actualSystemStatistic)) {
                            throw new Error("Invalid content of saved system statistics");
                        }
                    }
                    systemStatisticsDao.removeAll(function (isError, numberOfRemoved) {
                        if (isError) {
                            throw new Error("Problem with removing all system statistics: " + isError);
                        }
                        done();
                    });
                });
            });
        });

        it('should save multiple system statistics', function (done) {
            var expectedSystemStatistic0 = createFullTestSystemStatistic(0);
            var expectedSystemStatistic1 = createFullTestSystemStatistic(1);
            systemStatisticsDao.save(expectedSystemStatistic0, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving system statistic: " + isError);
                }
                systemStatisticsDao.save(expectedSystemStatistic1, function(isError) {
                    if (isError) {
                        throw new Error("Problem with saving system statistic: " + isError);
                    }
                    systemStatisticsDao.findAll(function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding all system statistics: " + isError);
                        } else {
                            console.log("Number of saved system statistics: " + result.length + "  ");
                            if(result.length != 2) {
                                throw new Error("Invalid number of retrieved system statistics");
                            }
                            var actualSystemStatistic0 = result[0];
                            var actualSystemStatistic1 = result[1];
                            if (!expectedSystemStatistic0.equals(actualSystemStatistic0)) {
                                throw new Error("Invalid content of saved system statistic");
                            }
                            if (!expectedSystemStatistic1.equals(actualSystemStatistic1)) {
                                throw new Error("Invalid content of saved system statistic");
                            }
                        }
                        systemStatisticsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all system statistics: " + isError);
                            }
                            done();
                        });
                    });
                });
            });
        });

    });

    describe('#findByDateRange()', function () {
        it('should find system statistic by timestamp range', function (done) {
            var expectedSystemStatistic = createFullTestSystemStatistic(0);
            var expectedSystemStatisticWhichShouldNotBeFound = createFullTestSystemStatistic(10);
            systemStatisticsDao.save(expectedSystemStatistic, function (isError) {
                if (isError) {
                    throw new Error("Problem with saving system statistics: " + isError);
                }
                systemStatisticsDao.save(expectedSystemStatisticWhichShouldNotBeFound, function(isError){
                    if (isError) {
                        throw new Error("Problem with saving system statistics: " + isError);
                    }
                    var from = moment().startOf('day');
                    var to = moment(from).add(1, 'days');
                    systemStatisticsDao.findByDateRange(from.toDate(), to.toDate(), function (isError, result) {
                        if (isError) {
                            throw new Error("Problem with finding system statistic by timestamp range: " + isError);
                        } else {
                            if(result.length != 1) {
                                throw new Error("Problem with finding system statistics by timestamp range");
                            }
                            var actualSystemStatistic = result[0];
                            if (!expectedSystemStatistic.equals(actualSystemStatistic)) {
                                throw new Error("Invalid content of saved system statistics");
                            }
                        }
                        systemStatisticsDao.removeAll(function (isError, numberOfRemoved) {
                            if (isError) {
                                throw new Error("Problem with removing all system statistics");
                            }
                            done();
                        });
                    });
                });
            });
        });
    });

    after(function(done) {
        mongoose.disconnect();
        done();
    })

});