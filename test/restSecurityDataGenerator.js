var moment = require("moment");
var uuidGenerator = require("node-uuid");
var cryptoJsSHA3 = require("crypto-js/sha3");
var cryptoJs = require("crypto-js");
var passwordHash = require("password-hash");

//var hashOfRequestUrl = passwordHash.generate('/users/password/reset', {algorithm: "sha1"});
//var hashOfRequestUrl2 = passwordHash.generate('/users/password/reset', {algorithm: "sha1"});

var hashOfRequestUrl = cryptoJsSHA3('/users/password/reset');
var hashOfRequestUrl2 = cryptoJsSHA3('/users/password/reset');

console.log("Encrypting with hash: " + hashOfRequestUrl.toString());
console.log("Encrypting with hash: " + hashOfRequestUrl2.toString());
var stringToEncrypt =  '7df62060-3629-11e6-8 ' + new Date().getTime() + ' ' + hashOfRequestUrl;
//var encryptedHash = cryptoJs.AES.encrypt(stringToEncrypt, hashOfRequestUrl);
var encryptedHash = cryptoJs.AES.encrypt(stringToEncrypt, hashOfRequestUrl.toString());

console.log("[" + encryptedHash.toString() + "]");

var bytes  = cryptoJs.AES.decrypt(encryptedHash.toString(), hashOfRequestUrl2.toString());
var plaintext = bytes.toString(cryptoJs.enc.Utf8);

console.log(plaintext);