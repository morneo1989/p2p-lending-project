module.exports = {
    createRejectedLoanApplication: function () {
        return {
            borrowerFk: null,
            claimedAmount: null,
            claimedPayoffPeriodInMonths: null,
            claimedRateOfLoan: null,
            timestamp: null,
            timestampOfRejection: null,
            equals: function (anotherRejectedLoanApplication) {
                return !(this.borrowerFk !== anotherRejectedLoanApplication.borrowerFk
                || this.claimedAmount !== anotherRejectedLoanApplication.claimedAmount
                || this.claimedPayoffPeriodInMonths !== anotherRejectedLoanApplication.claimedPayoffPeriodInMonths
                || this.claimedRateOfLoan !== anotherRejectedLoanApplication.claimedRateOfLoan
                || this.timestamp !== anotherRejectedLoanApplication.timestamp
                || this.timestampOfRejection !== anotherRejectedLoanApplication.timestampOfRejection);

            }
        };
    }
};