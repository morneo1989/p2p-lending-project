var agreementsModel = require("../main/agreements");

module.exports = {
    createHistoricalChangeInAgreement: function () {
        return {
            agreementFk: null,
            timestampOfAttributeChanges: null,
            attributesWhichChanged: agreementsModel.createAgreement(),
            equals: function (anotherHistoricalChangeInAgreement) {
                if (this.attributesWhichChanged != null && anotherHistoricalChangeInAgreement.attributesWhichChanged != null) {
                    if (!this.attributesWhichChanged.equals(anotherHistoricalChangeInAgreement.attributesWhichChanged)) {
                        return false;
                    }
                } else if(this.attributesWhichChanged == null || anotherHistoricalChangeInAgreement.attributesWhichChanged == null) {
                    return false;
                }
                if(this.agreementFk !== anotherHistoricalChangeInAgreement.agreementFk
                    || this.timestampOfAttributeChanges !== anotherHistoricalChangeInAgreement.timestampOfAttributeChanges) {
                    return false;
                }
                return true;
            }
        };
    }
};