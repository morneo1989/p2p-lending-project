var auctionsModel = require("../main/auctions");

module.exports = {
    createHistoricalChangeInAuction: function () {
        return {
            auctionFk: null,
            timestampOfAttributeChanges: null,
            attributesWhichChanged: auctionsModel.createAuction(),
            equals: function (anotherHistoricalChangeInAuction) {
                if (this.attributesWhichChanged != null && anotherHistoricalChangeInAuction.attributesWhichChanged != null) {
                    if (!this.attributesWhichChanged.equals(anotherHistoricalChangeInAuction.attributesWhichChanged)) {
                        return false;
                    }
                } else if(this.attributesWhichChanged == null || anotherHistoricalChangeInAuction.attributesWhichChanged == null) {
                    return false;
                }
                if(this.auctionFk !== anotherHistoricalChangeInAuction.auctionFk
                    || this.timestampOfAttributeChanges !== anotherHistoricalChangeInAuction.timestampOfAttributeChanges) {
                    return false;
                }
                return true;
            }
        };
    }
};