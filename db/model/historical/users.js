var userModel = require("../main/users");

module.exports = {
    createHistoricalChangeInUser: function () {
        return {
            userFk: null,
            timestampOfAttributeChanges: null,
            attributesWhichChanged: userModel.createUser(),
            equals: function (anotherHistoricalChangeInUser) {
                if (this.attributesWhichChanged != null && anotherHistoricalChangeInUser.attributesWhichChanged != null) {
                    if (!this.attributesWhichChanged.equals(anotherHistoricalChangeInUser.attributesWhichChanged)) {
                        return false;
                    }
                } else if(this.attributesWhichChanged == null || anotherHistoricalChangeInUser.attributesWhichChanged == null) {
                    return false;
                }
                if(this.userFk !== anotherHistoricalChangeInUser.userFk || this.timestampOfAttributeChanges !== anotherHistoricalChangeInUser.timestampOfAttributeChanges) {
                    return false;
                }
                return true;
            }
        };
    }
};