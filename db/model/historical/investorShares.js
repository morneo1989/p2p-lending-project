var investorSharesModel = require("../main/investorShares");

module.exports = {
    createHistoricalChangeInInvestorShare: function () {
        return {
            investorShareFk: null,
            timestampOfAttributeChanges: null,
            attributesWhichChanged: investorSharesModel.createInvestorShare(),
            equals: function (anotherHistoricalChangeInInvestorShare) {
                if (this.attributesWhichChanged != null && anotherHistoricalChangeInInvestorShare.attributesWhichChanged != null) {
                    if (!this.attributesWhichChanged.equals(anotherHistoricalChangeInInvestorShare.attributesWhichChanged)) {
                        return false;
                    }
                } else if(this.attributesWhichChanged == null || anotherHistoricalChangeInInvestorShare.attributesWhichChanged == null) {
                    return false;
                }
                if(this.investorShareFk !== anotherHistoricalChangeInInvestorShare.investorShareFk
                    || this.timestampOfAttributeChanges !== anotherHistoricalChangeInInvestorShare.timestampOfAttributeChanges) {
                    return false;
                }
                return true;
            }
        };
    }
};