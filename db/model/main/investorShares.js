module.exports = {
    createInvestorShare: function () {
        return {
            amountOfInvestment: null,
            investmentConfirmed: null,
            investorFk: null,
            auctionFk: null,
            investmentAtAfterMarket: null,
            timestamp: null,
            equals: function (anotherInvestorShare) {
                return !(this.amountOfInvestment !== anotherInvestorShare.amountOfInvestment || this.investmentConfirmed !== anotherInvestorShare.investmentConfirmed
                || this.investorFk !== anotherInvestorShare.investorFk || this.auctionFk !== anotherInvestorShare.auctionFk
                || this.investmentAtAfterMarket !== anotherInvestorShare.investmentAtAfterMarket || this.timestamp !== anotherInvestorShare.timestamp);

            }
        }
    }
};