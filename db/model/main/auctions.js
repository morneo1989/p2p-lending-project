var auctionsSchema = require("../../schema/main/auctions");

module.exports = {
    loanTargets: auctionsSchema.loanTargets,
    createAuction: function () {
        return {
            title: null,
            description: null,
            loanTarget: null,
            loanAmount: null,
            rateOfLoan: null,
            payOffSchedule: {
                paidLoanAmount: null,
                installments: []
            },
            timestampOfCreation: null,
            timestampOfRejection: null,
            timestampOfStart: null,
            timestampOfFinish: null,
            borrowerFk: null,
            borrowerApplication: {
                claimedAmount: null,
                claimedPayoffPeriodInMonths: null,
                claimedRateOfLoan: null,
                timestamp: null
            },
            equals: function (anotherAuction) {
                if (this.loanTarget !== anotherAuction.loanTarget || this.loanAmount !== anotherAuction.loanAmount ||
                    this.rateOfLoan !== anotherAuction.rateOfLoan || this.timestampOfCreation !== anotherAuction.timestampOfCreation ||
                    this.timestampOfRejection !== anotherAuction.timestampOfRejection || this.timestampOfStart !== anotherAuction.timestampOfStart ||
                    this.timestampOfFinish !== anotherAuction.timestampOfFinish) {
                    return false;
                }
                if (this.payOffSchedule != null && anotherAuction.payOffSchedule != null) {
                    if (this.payOffSchedule.paidLoanAmount !== anotherAuction.payOffSchedule.paidLoanAmount) {
                        return false;
                    }
                } else if (this.payOffSchedule == null || anotherAuction.payOffSchedule == null) {
                    return false;
                }
                if (this.borrowerApplication != null && anotherAuction.borrowerApplication != null) {
                    if (this.borrowerApplication.claimedAmount !== anotherAuction.borrowerApplication.claimedAmount ||
                        this.borrowerApplication.claimedPayoffPeriodInMonths !== anotherAuction.borrowerApplication.claimedPayoffPeriodInMonths ||
                        this.borrowerApplication.claimedRateOfLoan !== anotherAuction.borrowerApplication.claimedRateOfLoan ||
                        this.borrowerApplication.timestamp !== anotherAuction.borrowerApplication.timestamp) {
                        return false;
                    }
                } else if (this.borrowerApplication == null || anotherAuction.borrowerApplication == null) {
                    return false;
                }
                return true;
            }
        }
    }
};