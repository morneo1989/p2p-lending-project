var agreementSchema = require("../../schema/main/agreements");

module.exports = {
    agreementTypes: agreementSchema.agreementTypes,
    createAgreement: function () {
        return {
            title: null,
            document: null,
            type: null,
            stakeholdersFk: [],
            timestampOfCreation: null,
            equals: function (anotherAgreement) {
                if (this.title !== anotherAgreement.title || this.document !== anotherAgreement.document
                    || this.type !== anotherAgreement.type || this.timestampOfCreation !== anotherAgreement.timestampOfCreation) {
                    return false;
                }
                if(this.stakeholdersFk == null && anotherAgreement.stakeholdersFk == null) {
                    return true;
                } else if(this.stakeholdersFk != null && anotherAgreement.stakeholdersFk != null) {
                    return this.stakeholdersFk.sort().toString() === anotherAgreement.stakeholdersFk.sort().toString();
                } else {
                    return false;
                }

            }
        }
    }
};