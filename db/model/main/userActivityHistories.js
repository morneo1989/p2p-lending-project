var userActivityHistorySchema = require("../../schema/main/userActivityHistories");

module.exports = {
    userActivityTypes: userActivityHistorySchema.userActivityTypes,
    createActivityHistory: function () {
        return {
            userFk: null,
            activityType: null,
            activityParameters: [],
            timestamp: null,
            equals: function (anotherActivityHistory) {
                if (this.userFk !== anotherActivityHistory.userFk || this.activityType !== anotherActivityHistory.activityType
                    || this.timestamp !== anotherActivityHistory.timestamp) {
                    return false;
                }
                return true;
            }
        }
    }
};