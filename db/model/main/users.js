var userSchema = require("../../schema/main/users");
var passwordHash = require("password-hash");

module.exports = {
    userRoles: userSchema.userRoles,
    maritalStatuses: userSchema.maritalStatuses,
    employmentAreas: userSchema.employmentAreas,
    bankAccount: {
        operationType: userSchema.bankAccount.operationType
    },
    hashPassword: function(password) {
        return passwordHash.generate(password, {algorithm: "sha1", saltLength: 16, iterations: 2});
    },
    verifyPassword: function(plainPassword, hashedPassword) {
        return passwordHash.verify(plainPassword, hashedPassword);
    },
    createUser: function () {
        return {
            /* Common information */
            role: null,
            email: null,
            emailVerification: {
                isVerified: null,
                code: null,
                verificationSentTimestamp: null
            },
            password: null,
            passwordResetCode: null,
            phone: null,
            /* Borrower information */
            name: null,
            surname: null,
            socialIdentificationNumber: null,
            registeredAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            },
            correspondenceAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            },
            category: {
                name: null,
                loanAmountCoefficient: null,
                payoffPeriodCoefficient: null
            },
            ratingParameters: {
                age: null,
                employmentArea: null,
                maritalStatus: null,
                maritalStatusPeriodInMonths: null,
                incomeSource: null,
                monthlyAverageIncome: null,
                numberOfPeopleDependent: null,
                currentDebt: null,
                numberOfPaidOffLoans: null,
                numberOfNonPaidOffLoans: null
            },
            /* Investor information */
            company: {
                name: null,
                form: null,
                taxId: null,
                registeredAddress: {
                    street: null,
                    flatNumber: null,
                    houseNumber: null,
                    city: null,
                    postalCode: null
                },
                correspondenceAddress: {
                    street: null,
                    flatNumber: null,
                    houseNumber: null,
                    city: null,
                    postalCode: null
                }
            },
            representingPerson: {
                name: null,
                surname: null,
                email: null,
                socialIdentificationNumber: null
            },
            autobid: {
                interestMinRateOfLoan: null,
                interestMaxRateOfLoan: null,
                loanMinAmount: null,
                loanMaxAmount: null,
                loanMaxRepaymentPeriodInMonths: null
            },
            bankAccount: {
                number: null,
                currentIncome: null,
                currentBalance: null,
                timestampOfCreation: null,
                timestampOfValidation: null,
                pendingBankTransfers: null,
                confirmedBankTransfers: null
            },
            timestampOfCreation: null,
            equals: function (anotherUser) {
                if (this.role !== anotherUser.role || this.email !== anotherUser.email ||
                    this.password !== anotherUser.password || this.passwordResetCode !== anotherUser.passwordResetCode || this.name !== anotherUser.name ||
                    this.surname !== anotherUser.surname || this.timestampOfCreation !== anotherUser.timestampOfCreation) {
                    console.log("false1");
                    return false;
                }
                if (this.ratingparameters != null && anotherUser.ratingparameters != null) {
                    if (this.ratingparameters.age !== anotherUser.ratingparameters.age ||
                        this.ratingparameters.employmentArea !== anotherUser.ratingparameters.employmentArea ||
                        this.ratingparameters.maritialStatus !== anotherUser.ratingparameters.maritialStatus ||
                        this.ratingparameters.maritialStatusPeriodInMonths !== anotherUser.ratingparameters.maritialStatusPeriodInMonths ||
                        this.ratingparameters.numberOfNonPaidOffLoans !== anotherUser.ratingparameters.numberOfNonPaidOffLoans ||
                        this.ratingparameters.numberOfPaidOffLoans !== anotherUser.ratingparameters.numberOfPaidOffLoans) {
                        return false;
                    }
                } else if(this.ratingparameters == null || anotherUser.ratingparameters == null) {
                    return false;
                }
                if(this.autobid != null && anotherUser.autobid != null) {
                    if (this.autobid.interestMaxRateOfLoan !== anotherUser.autobid.interestMaxRateOfLoan ||
                        this.autobid.interestMinRateOfLoan !== anotherUser.autobid.interestMinRateOfLoan ||
                        this.autobid.loanMinAmount !== anotherUser.autobid.loanMinAmount ||
                        this.autobid.loanMaxAmount !== anotherUser.autobid.loanMaxAmount ||
                        this.autobid.loanMaxRepaymentPeriodInMonths !== anotherUser.autobid.loanMaxRepaymentPeriodInMonths) {
                        return false;
                    }
                } else if(this.autobid == null || anotherUser.autobid == null) {
                    return false;
                }
                if(this.bankAccount != null && anotherUser.bankAccount != null) {
                    if (this.bankAccount.number !== anotherUser.bankAccount.number ||
                        this.bankAccount.currentIncome !== anotherUser.bankAccount.currentIncome ||
                        this.bankAccount.currentBalance !== anotherUser.bankAccount.currentBalance ||
                        this.bankAccount.timestampOfCreation !== anotherUser.bankAccount.timestampOfCreation ||
                        this.bankAccount.timestampOfValidation !== anotherUser.bankAccount.timestampOfValidation) {
                        return false;
                    }
                } else if(this.bankAccount == null || anotherUser.bankAccount == null) {
                    return false;
                }
                return true;
            }
        }
    }
};