module.exports = {
    createUserComment: function () {
        return {
            userFk: null,
            platformUsageRating: null,
            commentModerated: null,
            comment: null,
            timestamp: null,
            equals: function (anotherUserComment) {
                if (this.userFk !== anotherUserComment.userFk || this.platformUsageRating !== anotherUserComment.platformUsageRating
                    || this.commentModerated !== anotherUserComment.commentModerated || this.comment !== anotherUserComment.comment
                    || this.timestamp !== anotherUserComment.timestamp) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }
};