module.exports = {
    createPhoneVerification: function () {
        return {
            email: null,
            phoneNumber: null,
            phoneVerificationCode: null,
            equals: function (anotherPhoneVerification) {
                if (this.email !== anotherPhoneVerification.email || this.phoneNumber !== anotherPhoneVerification.phoneNumber
                    || this.phoneVerificationCode !== anotherPhoneVerification.phoneVerificationCode) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }
};