module.exports = {
    createSystemStatistic: function () {
        return {
            openedAuctionsAmount: null,
            paidAuctionsAmount: null,
            notPaidAuctionsAmount: null,
            cashFlowAmount: null,
            usersAmount: null,
            rejectedLoanApplicationsAmount: null,
            timestampOfStatisticsSnapshot: null,
            equals: function (anotherSystemStatistic) {
                if(this.openedAuctionsAmount !== anotherSystemStatistic.openedAuctionsAmount
                || this.paidAuctionsAmount !== anotherSystemStatistic.paidAuctionsAmount
                || this.notPaidAuctionsAmount !== anotherSystemStatistic.notPaidAuctionsAmount
                || this.cashFlowAmount !== anotherSystemStatistic.cashFlowAmount
                || this.usersAmount !== anotherSystemStatistic.usersAmount
                || this.rejectedLoanApplicationsAmount !== anotherSystemStatistic.rejectedLoanApplicationsAmount) {
                    return false;
                }
                if(this.timestampOfStatisticsSnapshot != null && anotherSystemStatistic.timestampOfStatisticsSnapshot != null) {
                    if(this.timestampOfStatisticsSnapshot.getTime() !== anotherSystemStatistic.timestampOfStatisticsSnapshot.getTime()) {
                        return false;
                    }
                } else if(this.timestampOfStatisticsSnapshot != null || anotherSystemStatistic.timestampOfStatisticsSnapshot != null) {
                    return false;
                }
                return true;
            }
        };
    }
};