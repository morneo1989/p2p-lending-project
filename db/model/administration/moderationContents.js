module.exports = {
    createLegalRegulation: function () {
        return {
            contentName: 'legalRegulation',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    },
    createFaq: function () {
        return {
            contentName: 'FAQ',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            faqQuestion: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                    || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    },
    createP2PAbout: function () {
        return {
            contentName: 'P2P_About',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                    || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    },
    createInvestGuide: function () {
        return {
            contentName: 'Invest_Guide',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                    || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    },
    createBorrowGuide: function () {
        return {
            contentName: 'Borrow_Guide',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                    || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    },
    createContact: function () {
        return {
            contentName: 'Contact',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                    || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    },
    createAboutUs: function () {
        return {
            contentName: 'About_Us',
            legalRegulationTitle: null,
            htmlFormattedText: null,
            equals: function (anotherLegalRegulation) {
                if(this.legalRegulationTitle !== anotherLegalRegulation.legalRegulationTitle
                    || this.htmlFormattedText !== anotherLegalRegulation.htmlFormattedText) {
                    return false;
                }
                return true;
            }
        };
    }
};