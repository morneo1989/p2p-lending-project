var historicalChangesInAgreements = require("../../schema/historical/agreements");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAgreementsDAO", "find all historical changes in all agreements");
        var historicalChangesInAgreementsModel = this.mongoose.model(historicalChangesInAgreements.historicalChangesInAgreementsCollectionName, historicalChangesInAgreements.historicalChangesInAgreementsSchema);
        historicalChangesInAgreementsModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAgreementsDAO", "find historical changes in agreement with: " + JSON.stringify(parameters));
        var historicalChangesInAgreementsModel = this.mongoose.model(historicalChangesInAgreements.historicalChangesInAgreementsCollectionName, historicalChangesInAgreements.historicalChangesInAgreementsSchema);
        historicalChangesInAgreementsModel.find(parameters, callbackFunction);
    },

    save: function (historicalChangeInAgreementToSave, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAgreementsDAO", "save historical change in agreement: " + JSON.stringify(historicalChangeInAgreementToSave));
        var historicalChangesInAgreementsModel = this.mongoose.model(historicalChangesInAgreements.historicalChangesInAgreementsCollectionName, historicalChangesInAgreements.historicalChangesInAgreementsSchema);
        var newHistoricalChangeInAgreement = new historicalChangesInAgreementsModel(historicalChangeInAgreementToSave);
        newHistoricalChangeInAgreement.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAgreementsDAO", "remove all historical changes in all agreements");
        var historicalChangesInAgreementsModel = this.mongoose.model(historicalChangesInAgreements.historicalChangesInAgreementsCollectionName, historicalChangesInAgreements.historicalChangesInAgreementsSchema);
        historicalChangesInAgreementsModel.remove(callbackFunction);
    }

};