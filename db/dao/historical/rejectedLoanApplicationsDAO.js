var rejectedLoanApplications = require("../../schema/historical/rejectedLoanApplications");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function (callbackFunction) {
        consoleLogger.logDebug("rejectedLoanApplicationsDAO", "find all historical changes in all auctions");
        var rejectedLoanApplicationsModel = this.mongoose.model(rejectedLoanApplications.rejectedLoanApplicationsCollectionName,
            rejectedLoanApplications.rejectedLoanApplicationsSchema);
        rejectedLoanApplicationsModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        consoleLogger.logDebug("rejectedLoanApplicationsDAO", "find historical changes in auction with: " + JSON.stringify(parameters));
        var rejectedLoanApplicationsModel = this.mongoose.model(rejectedLoanApplications.rejectedLoanApplicationsCollectionName,
            rejectedLoanApplications.rejectedLoanApplicationsSchema);
        rejectedLoanApplicationsModel.find(parameters, callbackFunction);
    },

    save: function (rejectedLoanApplicationToSave, callbackFunction) {
        consoleLogger.logDebug("rejectedLoanApplicationsDAO", "save historical change in auction: " + JSON.stringify(rejectedLoanApplicationToSave));
        var rejectedLoanApplicationsModel = this.mongoose.model(rejectedLoanApplications.rejectedLoanApplicationsCollectionName,
            rejectedLoanApplications.rejectedLoanApplicationsSchema);
        var newRejectedLoanApplication = new rejectedLoanApplicationsModel(rejectedLoanApplicationToSave);
        newRejectedLoanApplication.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        consoleLogger.logDebug("rejectedLoanApplicationsDAO", "remove all historical changes in all auctions");
        var rejectedLoanApplicationsModel = this.mongoose.model(rejectedLoanApplications.rejectedLoanApplicationsCollectionName,
            rejectedLoanApplications.rejectedLoanApplicationsSchema);
        rejectedLoanApplicationsModel.remove(callbackFunction);
    }

};