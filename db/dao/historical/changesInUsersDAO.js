var historicalChangesInUsers = require("../../schema/historical/users");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInUsersDAO", "find all historical changes in all users");
        var historicalUsersModel = this.mongoose.model(historicalChangesInUsers.historicalChangesInUsersCollectionName, historicalChangesInUsers.historicalChangesInUsersSchema);
        historicalUsersModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInUsersDAO", "find historical changes in user with: " + JSON.stringify(parameters));
        var historicalUsersModel = this.mongoose.model(historicalChangesInUsers.historicalChangesInUsersCollectionName, historicalChangesInUsers.historicalChangesInUsersSchema);
        historicalUsersModel.find(parameters, callbackFunction);
    },

    save: function (historicalChangeInUserToSave, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInUsersDAO", "save historical change in user: " + JSON.stringify(historicalChangeInUserToSave));
        var historicalUsersModel = this.mongoose.model(historicalChangesInUsers.historicalChangesInUsersCollectionName, historicalChangesInUsers.historicalChangesInUsersSchema);
        var newHistoricalChangeInUser = new historicalUsersModel(historicalChangeInUserToSave);
        newHistoricalChangeInUser.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInUsersDAO", "remove all historical changes in all users");
        var historicalUsersModel = this.mongoose.model(historicalChangesInUsers.historicalChangesInUsersCollectionName, historicalChangesInUsers.historicalChangesInUsersSchema);
        historicalUsersModel.remove(callbackFunction);
    }

};