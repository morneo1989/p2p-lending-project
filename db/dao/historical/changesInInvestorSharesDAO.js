var historicalChangesInInvestorShares = require("../../schema/historical/investorShares");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInInvestorSharesDAO", "find all historical changes in all investor share");
        var historicalChangesInInvestorSharesModel = this.mongoose.model(historicalChangesInInvestorShares.historicalChangesInInvestorSharesCollectionName,
            historicalChangesInInvestorShares.historicalChangesInInvestorSharesSchema);
        historicalChangesInInvestorSharesModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInInvestorSharesDAO", "find historical changes in investor share with: " + JSON.stringify(parameters));
        var historicalChangesInInvestorSharesModel = this.mongoose.model(historicalChangesInInvestorShares.historicalChangesInInvestorSharesCollectionName,
            historicalChangesInInvestorShares.historicalChangesInInvestorSharesSchema);
        historicalChangesInInvestorSharesModel.find(parameters, callbackFunction);
    },

    save: function (historicalChangeInInvestorShareToSave, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInInvestorSharesDAO", "save historical change in investor share: " + JSON.stringify(historicalChangeInInvestorShareToSave));
        var historicalChangesInInvestorSharesModel = this.mongoose.model(historicalChangesInInvestorShares.historicalChangesInInvestorSharesCollectionName,
            historicalChangesInInvestorShares.historicalChangesInInvestorSharesSchema);
        var newHistoricalChangeInInvestorShare = new historicalChangesInInvestorSharesModel(historicalChangeInInvestorShareToSave);
        newHistoricalChangeInInvestorShare.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInInvestorSharesDAO", "remove all historical changes in all investor shares");
        var historicalChangesInInvestorSharesModel = this.mongoose.model(historicalChangesInInvestorShares.historicalChangesInInvestorSharesCollectionName,
            historicalChangesInInvestorShares.historicalChangesInInvestorSharesSchema);
        historicalChangesInInvestorSharesModel.remove(callbackFunction);
    }

};