var historicalChangesInAuctions = require("../../schema/historical/auctions");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAuctionsDAO", "find all historical changes in all auctions");
        var historicalChangesInAuctionsModel = this.mongoose.model(historicalChangesInAuctions.historicalChangesInAuctionsCollectionName,
            historicalChangesInAuctions.historicalChangesInAuctionsSchema);
        historicalChangesInAuctionsModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAuctionsDAO", "find historical changes in auction with: " + JSON.stringify(parameters));
        var historicalChangesInAuctionsModel = this.mongoose.model(historicalChangesInAuctions.historicalChangesInAuctionsCollectionName,
            historicalChangesInAuctions.historicalChangesInAuctionsSchema);
        historicalChangesInAuctionsModel.find(parameters, callbackFunction);
    },

    save: function (historicalChangeInAuctionToSave, callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAuctionsDAO", "save historical change in auction: " + JSON.stringify(historicalChangeInAuctionToSave));
        var historicalChangesInAuctionsModel = this.mongoose.model(historicalChangesInAuctions.historicalChangesInAuctionsCollectionName,
            historicalChangesInAuctions.historicalChangesInAuctionsSchema);
        var newHistoricalChangeInAuction = new historicalChangesInAuctionsModel(historicalChangeInAuctionToSave);
        newHistoricalChangeInAuction.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        consoleLogger.logDebug("historicalChangesInAuctionsDAO", "remove all historical changes in all auctions");
        var historicalChangesInAuctionsModel = this.mongoose.model(historicalChangesInAuctions.historicalChangesInAuctionsCollectionName,
            historicalChangesInAuctions.historicalChangesInAuctionsSchema);
        historicalChangesInAuctionsModel.remove(callbackFunction);
    }

};