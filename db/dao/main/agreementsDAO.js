var agreements = require("../../schema/main/agreements");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var agreementsModel = this.mongoose.model(agreements.agreementsCollectionName, agreements.agreementsSchema);
        agreementsModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var agreementsModel = this.mongoose.model(agreements.agreementsCollectionName, agreements.agreementsSchema);
        agreementsModel.find(parameters, callbackFunction);
    },

    save: function (userToSave, callbackFunction) {
        var agreementsModel = this.mongoose.model(agreements.agreementsCollectionName, agreements.agreementsSchema);
        var newAgreement = new agreementsModel(userToSave);
        newAgreement.save(callbackFunction);
    },

    updateBy: function (parameters, attributesToUpdate, callbackFunction) {
        var agreementsModel = this.mongoose.model(agreements.agreementsCollectionName, agreements.agreementsSchema);
        agreementsModel.update(parameters, attributesToUpdate, {multi: true}, callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var agreementsModel = this.mongoose.model(agreements.agreementsCollectionName, agreements.agreementsSchema);
        agreementsModel.remove(callbackFunction);
    }

};