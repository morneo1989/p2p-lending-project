var userComments = require("../../schema/main/userComments");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var userCommentsModel = this.mongoose.model(userComments.userCommentsCollectionName, userComments.userCommentsSchema);
        userCommentsModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var userCommentsModel = this.mongoose.model(userComments.userCommentsCollectionName, userComments.userCommentsSchema);
        userCommentsModel.find(parameters, callbackFunction);
    },

    save: function (userCommentToSave, callbackFunction) {
        var userCommentsModel = this.mongoose.model(userComments.userCommentsCollectionName, userComments.userCommentsSchema);
        var newUserComment = new userCommentsModel(userCommentToSave);
        newUserComment.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var userCommentsModel = this.mongoose.model(userComments.userCommentsCollectionName, userComments.userCommentsSchema);
        userCommentsModel.remove(callbackFunction);
    }

};
