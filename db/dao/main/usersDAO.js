var users = require("../../schema/main/users");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        usersModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        usersModel.find(parameters, callbackFunction);
    },

    /**
     * Method used to select chosen attributes of users. For that it accepts additional argument
     * @param mongoose object used for connection with Database
     * @param selectionProjection String used for selecting attributes for selection, e.g.: "name surname"
     * @param parameters
     * @param callbackFunction
     */
    findSelectedBy: function (selectionProjection, parameters, callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        usersModel.find(parameters, selectionProjection, callbackFunction);
    },

    save: function (userToSave, callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        var newUser = new usersModel(userToSave);
        newUser.save(callbackFunction);
    },

    updateBy: function (parameters, attributesToUpdate, callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        usersModel.update(parameters, attributesToUpdate, {multi: true}, callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        usersModel.remove(callbackFunction);
    },

    removeBy: function (parameters, callbackFunction) {
        var usersModel = this.mongoose.model(users.usersCollectionName, users.usersSchema);
        usersModel.remove(parameters, callbackFunction);
    }

};
