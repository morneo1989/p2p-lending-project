var investorShares = require("../../schema/main/investorShares");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var investorSharesModel = this.mongoose.model(investorShares.investorSharesCollectionName, investorShares.investorSharesSchema);
        investorSharesModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var investorSharesModel = this.mongoose.model(investorShares.investorSharesCollectionName, investorShares.investorSharesSchema);
        investorSharesModel.find(parameters, callbackFunction);
    },

    save: function (investorShareToSave, callbackFunction) {
        var investorSharesModel = this.mongoose.model(investorShares.investorSharesCollectionName, investorShares.investorSharesSchema);
        var newInvestorSharesModel = new investorSharesModel(investorShareToSave);
        newInvestorSharesModel.save(callbackFunction);
    },

    updateBy: function (parameters, attributesToUpdate, callbackFunction) {
        var investorSharesModel = this.mongoose.model(investorShares.investorSharesCollectionName, investorShares.investorSharesSchema);
        investorSharesModel.update(parameters, attributesToUpdate, {multi: true}, callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var investorSharesModel = this.mongoose.model(investorShares.investorSharesCollectionName, investorShares.investorSharesSchema);
        investorSharesModel.remove(callbackFunction);
    }

};