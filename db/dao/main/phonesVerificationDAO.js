var phoneVerifications = require("../../schema/main/phoneVerification");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        phonesModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        phonesModel.find(parameters, callbackFunction);
    },

    /**
     * Method used to select chosen attributes of users. For that it accepts additional argument
     * @param mongoose object used for connection with Database
     * @param selectionProjection String used for selecting attributes for selection, e.g.: "name surname"
     * @param parameters
     * @param callbackFunction
     */
    findSelectedBy: function (selectionProjection, parameters, callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        phonesModel.find(parameters, selectionProjection, callbackFunction);
    },

    save: function (phoneVerificationToSave, callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        var newPhoneVerification = new phonesModel(phoneVerificationToSave);
        newPhoneVerification.save(callbackFunction);
    },

    updateBy: function (parameters, attributesToUpdate, callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        phonesModel.update(parameters, attributesToUpdate, {multi: true}, callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        phonesModel.remove(callbackFunction);
    },

    removeBy: function (parameters, callbackFunction) {
        var phonesModel = this.mongoose.model(phoneVerifications.phoneVerificationsCollectionName, phoneVerifications.phoneVerificationsSchema);
        phonesModel.remove(parameters, callbackFunction);
    }

};
