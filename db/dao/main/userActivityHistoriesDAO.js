var userActivityHistories = require("../../schema/main/userActivityHistories");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var userActivityHistoriesModel = this.mongoose.model(userActivityHistories.userActivitiesCollectionName, userActivityHistories.userActivitiesSchema);
        userActivityHistoriesModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var userActivityHistoriesModel = this.mongoose.model(userActivityHistories.userActivitiesCollectionName, userActivityHistories.userActivitiesSchema);
        userActivityHistoriesModel.find(parameters, callbackFunction);
    },

    save: function (newUserActivityHistoryToSave, callbackFunction) {
        var userActivityHistoriesModel = this.mongoose.model(userActivityHistories.userActivitiesCollectionName, userActivityHistories.userActivitiesSchema);
        var newUserActivityHistory = new userActivityHistoriesModel(newUserActivityHistoryToSave);
        newUserActivityHistory.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var userActivityHistoriesModel = this.mongoose.model(userActivityHistories.userActivitiesCollectionName, userActivityHistories.userActivitiesSchema);
        userActivityHistoriesModel.remove(callbackFunction);
    }

};