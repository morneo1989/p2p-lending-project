var auctions = require("../../schema/main/auctions");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAll: function(callbackFunction) {
        var auctionsModel = this.mongoose.model(auctions.auctionsCollectionName, auctions.auctionsSchema);
        auctionsModel.find(callbackFunction);
    },

    findBy: function (parameters, callbackFunction) {
        var auctionsModel = this.mongoose.model(auctions.auctionsCollectionName, auctions.auctionsSchema);
        auctionsModel.find(parameters, callbackFunction);
    },

    save: function (auctionToSave, callbackFunction) {
        var auctionsModel = this.mongoose.model(auctions.auctionsCollectionName, auctions.auctionsSchema);
        var newAuction = new auctionsModel(auctionToSave);
        newAuction.save(callbackFunction);
    },

    updateBy: function (parameters, attributesToUpdate, callbackFunction) {
        var auctionsModel = this.mongoose.model(auctions.auctionsCollectionName, auctions.auctionsSchema);
        auctionsModel.update(parameters, attributesToUpdate, {multi: true}, callbackFunction);
    },

    removeAll: function (callbackFunction) {
        var auctionsModel = this.mongoose.model(auctions.auctionsCollectionName, auctions.auctionsSchema);
        auctionsModel.remove(callbackFunction);
    },

    removeBy: function (parameters, callbackFunction) {
        var auctionsModel = this.mongoose.model(auctions.auctionsCollectionName, auctions.auctionsSchema);
        auctionsModel.remove(callbackFunction);
    }

};
