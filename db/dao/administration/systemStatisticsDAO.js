var systemStatistics = require("../../schema/administration/systemStatistics");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function(mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findAllOrderedDesc: function (callbackFunction) {
        consoleLogger.logDebug("systemStatisticsDAO", "find all system statistics");
        var systemStatisticsModel = this.mongoose.model(systemStatistics.systemStaticticsCollectionName,
            systemStatistics.systemStaticticsSchema);
        systemStatisticsModel.find().sort({timestampOfStatisticsSnapshot: -1}).exec(callbackFunction);
    },

    findByDateRange: function (from, to, callbackFunction) {
        consoleLogger.logDebug("systemStatisticsDAO", "find system statistic from: " + JSON.stringify(from) + " to:" + JSON.stringify(to));
        var systemStatisticsModel = this.mongoose.model(systemStatistics.systemStaticticsCollectionName,
            systemStatistics.systemStaticticsSchema);
        systemStatisticsModel.find({
            timestampOfStatisticsSnapshot: {
                $gte: from,
                $lt: to
            }
        }, callbackFunction);
    },

    save: function (systemStatisticToSave, callbackFunction) {
        consoleLogger.logDebug("systemStatisticsDAO", "save system statistic: " + JSON.stringify(systemStatisticToSave));
        var systemStatisticsModel = this.mongoose.model(systemStatistics.systemStaticticsCollectionName,
            systemStatistics.systemStaticticsSchema);
        var newSystemStatistic = new systemStatisticsModel(systemStatisticToSave);
        newSystemStatistic.save(callbackFunction);
    },

    removeAll: function (callbackFunction) {
        consoleLogger.logDebug("systemStatisticsDAO", "remove all system statistics");
        var systemStatisticsModel = this.mongoose.model(systemStatistics.systemStaticticsCollectionName,
            systemStatistics.systemStaticticsSchema);
        systemStatisticsModel.remove(callbackFunction);
    }

};