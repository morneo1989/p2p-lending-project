var moderationContent = require("../../schema/administration/moderation/moderationContent");
var consoleLogger = require("../../../core/logger/consoleLogger");

module.exports = {
    mongoose: null,

    init: function (mongooseConnect) {
        this.mongoose = mongooseConnect;
    },
    findByContentName: function(contentName, findCallback) {
        consoleLogger.logDebug("moderationContentDAO", "find content by contentType: " + contentName);
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        moderationContentsModel.find({'contentName': contentName} ,findCallback);
    },
    findAllByContentName: function(contentName, findCallback) {
        consoleLogger.logDebug("moderationContentDAO", "find content by contentType: " + contentName);
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        moderationContentsModel.find({'contentName': contentName} ,findCallback);
    },
    findAllLegalRegulationsWithIdAndTitle: function (findCallback) {
        consoleLogger.logDebug("moderationContentDAO", "find all legal regulations");
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        moderationContentsModel.find({'contentName': 'legalRegulation'}, "_id legalRegulationTitle", findCallback);
    },

    findById: function (id, findCallback) {
        consoleLogger.logDebug("moderationContentDAO", "find moderation content with id: " + id);
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        moderationContentsModel.find({
            _id: id
        }, findCallback);
    },

    save: function (moderationContentToSave, saveCallback) {
        consoleLogger.logDebug("moderationContentDAO", "save moderation content: " + JSON.stringify(moderationContentToSave));
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        var newLegalRegulation = new moderationContentsModel(moderationContentToSave);
        newLegalRegulation.save(saveCallback);
    },

    updateById: function (id, attributesToUpdate, updateCallback) {
        consoleLogger.logDebug("moderationContentDAO", "update moderation content: " + JSON.stringify(attributesToUpdate));
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        moderationContentsModel.update({_id: id}, attributesToUpdate, updateCallback);
    },

    removeAll: function (removeCallback) {
        consoleLogger.logDebug("moderationContentDAO", "remove all moderation contents");
        var moderationContentsModel = this.mongoose.model(moderationContent.moderationContentsCollectionName,
            moderationContent.moderationContentsSchema);
        moderationContentsModel.remove(removeCallback);
    }

};