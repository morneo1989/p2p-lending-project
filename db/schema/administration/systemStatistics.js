var mongoose = require("mongoose");

module.exports = {
    systemStaticticsCollectionName: "systemstatistics",
    systemStaticticsSchema: mongoose.Schema({
        openedAuctionsAmount: Number,
        paidAuctionsAmount: Number,
        notPaidAuctionsAmount: Number,
        cashFlowAmount: Number,
        usersAmount: Number,
        rejectedLoanApplicationsAmount: Number,
        timestampOfStatisticsSnapshot: Date
    })
};