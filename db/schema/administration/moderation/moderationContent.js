var mongoose = require("mongoose");

module.exports = {
    moderationContentsCollectionName: "moderation_content",
    moderationContentsSchema: mongoose.Schema({
        contentName: String,
        legalRegulationTitle: String,
        faqQuestion: String,
        htmlFormattedText: String
    })
};