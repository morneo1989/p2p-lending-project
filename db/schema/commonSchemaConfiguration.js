module.exports = {
    cashFlowBasicUnitInPLN: 20,
    databaseTransactionLikeSemanticStatuses: {
        NO_ACTIVE_DB_TRANSACTION: 0,
        ACTIVE_DB_TRANSACTION: 1,
        DB_TRANSACTION_TO_ROLLBACK: 2
    },
    databaseTransactionLikeSemanticInfo: {
        status: Number,
        timestamp: Date
    }
};