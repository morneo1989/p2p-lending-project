var mongoose = require("mongoose");
var investorSharesSchema = require("../main/investorShares");

module.exports = {
    historicalChangesInInvestorSharesCollectionName: "investorshares_historicalchanges",
    historicalChangesInInvestorSharesSchema: mongoose.Schema({
        investorShareFk: {type: String, index: true},
        timestampOfAttributeChanges: {type: Date, expires: 365*24*60*60},
        attributesWhichChanged: investorSharesSchema.investorSharesSchema
    })
};