var mongoose = require("mongoose");

module.exports = {
    rejectedLoanApplicationsCollectionName: "rejectedloanapplications",
    rejectedLoanApplicationsSchema: mongoose.Schema({
        borrowerFk: {type: String, index: true},
        claimedAmount: Number,
        claimedPayoffPeriodInMonths: Number,
        claimedRateOfLoan: Number,
        timestamp: Date,
        timestampOfRejection: {type: Date, expires: 365*24*60*60}
    })
};