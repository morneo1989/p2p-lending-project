var mongoose = require("mongoose");
var agreementsSchema = require("../main/agreements");

module.exports = {
    historicalChangesInAgreementsCollectionName: "agreements_historicalchanges",
    historicalChangesInAgreementsSchema: mongoose.Schema({
        agreementFk: {type: String, index: true},
        timestampOfAttributeChanges: {type: Date, expires: 100*24*60*60},
        attributesWhichChanged: agreementsSchema.agreementsSchema
    })
};