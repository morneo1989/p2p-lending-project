var mongoose = require("mongoose");
var userSchema = require("../main/users");

module.exports = {
    historicalChangesInUsersCollectionName: "users_historicalchanges",
    historicalChangesInUsersSchema: mongoose.Schema({
        userFk: {type: String, index: true},
        timestampOfAttributeChanges: {type: Date, expires: 365*24*60*60},
        attributesWhichChanged: userSchema.usersSchema
    })
};