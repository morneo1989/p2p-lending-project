var mongoose = require("mongoose");
var auctionsSchema = require("../main/auctions");

module.exports = {
    historicalChangesInAuctionsCollectionName: "auctions_historicalchanges",
    historicalChangesInAuctionsSchema: mongoose.Schema({
        auctionFk: {type: String, index: true},
        timestampOfAttributeChanges: {type: Date, expires: 365*24*60*60},
        attributesWhichChanged: auctionsSchema.auctionsSchema
    })
};