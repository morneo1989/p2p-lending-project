var mongoose = require("mongoose");

module.exports = {
    usersCollectionName: "users",
    userRoles: {
        ADMIN: "ADMIN",
        INVESTOR: "INVESTOR",
        BORROWER: "BORROWER"
    },
    maritalStatuses: [
        "MARRIED", "WIDOWED", "DIVORCED", "SINGLE"
    ],
    employmentAreas: [
        "IT", "FINANCE", "INSURANCE", "HEAVY INDUSTRY", "LIGHT INDUSTRY", "CULTURE AND ART",
        "COMMERCE AND E-COMMERCE", "POWER INDUSTRY AND HEAT", "OTHER STATE ADMINISTRATION", "EDUCATION",
        "MEDIA", "ENVIRONMENTAL PROTECTION", "MEDICINE", "MILITARY", "LAW", "POLICE", "STUDENT"
    ],
    companyForms: [
        "JOINT_STOCK_COMPANY", "LIMITED_RESPONSIBILITY_COMPANY", "GENERAL_PARTNERSHIP", "PARTNERSHIP_COMPANY",
        "LIMITED_PARTNERSHIP", "JOINT_STOCK_LIMITED_COMPANY", "NATIONAL_COMPANY"
    ],
    incomeSources: [
        "BUSINESS: ONE-MAN", "BUSINESS: PARTNER", "BUSINESS: RENT FLAT", "FIXED-TERM CONTRACT: CONTRACT WORK"
        , "FIXED-TERM CONTRACT: ORDER WORK", "FIXED-TERM CONTRACT: MANAGEMENT CONTRACT", "FIXED-TERM CONTRACT: CONTRACT WORK"
        , "FIXED-TERM CONTRACT: CONTRACT OF EMPLOYMENT", "INDEFINITE-TERM CONTRACT", "UNEMPLOYED", "FARMER", "HOUSE KEEPER"
        , "PENSION", "STUDENT"
    ],
    bankAccount: {
        operationType: {
            INCOMING: "INCOMING",
            OUTGOING: "OUTGOING"
        }
    },
    usersSchema: mongoose.Schema({
        /* Common information */
        role: String,
        email: String,
        emailVerification: {
            isVerified: Boolean,
            code: String,
            verificationSentTimestamp: Date
        },
        password: String,
        passwordResetCode: String,
        phone: String,
        bankAccount: {
            number: String,
            currentIncome: Number,
            currentBalance: Number,
            timestampOfCreation: Date,
            timestampOfValidation: Date,
            pendingBankTransfers: [{
                transferDetails: {
                    title: String
                    //TODO dokoncze razem z implementacja modulu integracji z systemem platnosci internetowych
                },
                cashAmount: Number,
                operationType: String,
                timestampOfCreation: Date
            }],
            confirmedBankTransfers: [{
                transferDetails: {
                    title: String
                    //TODO dokoncze razem z implementacja modulu integracji z systemem platnosci internetowych
                },
                cashAmount: Number,
                operationType: String,
                timestampOfConfirmation: Date
            }]
        },
        timestampOfCreation: Date,
        /* Borrower information */
        name: String,
        surname: String,
        socialIdentificationNumber: String,
        registeredAddress: {
            street: String,
            flatNumber: Number,
            houseNumber: Number,
            city: String,
            postalCode: String
        },
        correspondenceAddress: {
            street: String,
            flatNumber: Number,
            houseNumber: Number,
            city: String,
            postalCode: String
        },
        category: {
            name: String,
            loanAmountCoefficient: Number,
            payoffPeriodCoefficient: Number
        },
        ratingParameters: {
            age: Number,
            employmentArea: String,
            maritalStatus: String,
            maritalStatusPeriodInMonths: Number,
            incomeSource: String,
            monthlyAverageIncome: Number,
            numberOfPeopleDependent: Number,
            currentDebt: Number,
            numberOfPaidOffLoans: Number,
            numberOfNonPaidOffLoans: Number
        },
        /* Investor information */
        company: {
            name: String,
            form: String,
            taxId: String,
            registeredAddress: {
                street: String,
                flatNumber: Number,
                houseNumber: Number,
                city: String,
                postalCode: String
            },
            correspondenceAddress: {
                street: String,
                flatNumber: Number,
                houseNumber: Number,
                city: String,
                postalCode: String
            }
        },
        representingPerson: {
            name: String,
            surname: String,
            email: String,
            socialIdentificationNumber: String
        },
        autobid: {
            interestMinRateOfLoan: Number,
            interestMaxRateOfLoan: Number,
            loanMinAmount: Number,
            loanMaxAmount: Number,
            loanMaxRepaymentPeriodInMonths: Number
        }
    })
};