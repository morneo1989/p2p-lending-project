var mongoose = require("mongoose");

module.exports = {
    userCommentsCollectionName: "usercomments",
    userCommentsSchema: mongoose.Schema({
        userFk: {type: String, index: true},
        platformUsageRating: Number,
        commentModerated: Boolean,
        comment: String,
        timestamp: Date
    })
};