var mongoose = require("mongoose");

module.exports = {
    auctionsCollectionName: "auctions",
    loanTargets: [
        "CAR", "FLAT", "HOLIDAYS", "ELECTRONICS", "BUSINESS", "STUDY"
    ],
    auctionsSchema: mongoose.Schema({
        title: String,
        description: String,
        loanTarget: String,
        loanAmount: {type: Number, index: true},
        rateOfLoan: {type: Number, index: true},
        payOffSchedule: {
            paidLoanAmount: Number,
            installments: [{
                installmentAmount: Number,
                dateOfRepayment: Date
            }]
        },
        timestampOfCreation: Date,
        timestampOfRejection: Date,
        timestampOfStart: Date,
        timestampOfFinish: Date,
        borrowerFk: {type: String, index:true},
        borrowerApplication: {
            claimedAmount: Number,
            claimedPayoffPeriodInMonths: Number,
            claimedRateOfLoan: Number,
            timestamp: Date
        }
    })
};