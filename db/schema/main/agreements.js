var mongoose = require("mongoose");

module.exports = {
    agreementsCollectionName: "agreements",
    agreementTypes: ["LOAN_AGREEMENT", "AFTERMARKET_ASSIGNMENT_AGREEMENT"],
    agreementsSchema: mongoose.Schema({
        stakeholdersFk: {type: [String], index: true},
        title: String,
        document: Buffer,
        type: String,
        timestampOfCreation: Date
    })
};