var mongoose = require("mongoose");

module.exports = {
    phoneVerificationsCollectionName: "phones_verifications",
    phoneVerificationsSchema: mongoose.Schema({
        email: {type: String, index: true},
        phoneNumber: String,
        phoneVerificationCode: Number
    })
};