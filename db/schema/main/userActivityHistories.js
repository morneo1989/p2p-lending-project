var mongoose = require("mongoose");

module.exports = {
    userActivitiesCollectionName: "useractivities",
    userActivityTypes: {
        PERSONAL_DETAILS_UPDATE: 0,
        BORROWER_REGISTRATION_ATTEMPT: 1,
        VERIFIED_BORROWER_REGISTRATION: 2,
        AUCTION_CREATION: 3,
        AUCTION_START: 4,
        AUCTION_FINISH: 5,
        AUCTION_NO_START: 6,
        PAYOFF_INSTALLMENT_PAYMENT: 7,
        PAYOFF_INSTALLMENT_DELAY: 8,
        INVESTOR_INVESTMENT_CREATION: 9,
        INVESTOR_INVESTMENT_ROLLBACK: 10,
        INVESTOR_INVESTMENT_START: 11,
        INVESTOR_INVESTMENT_PAYOFF: 12,
        INVESTOR_INVESTMENT_AFTERMARKET_MOVEMENT: 13,
        INVESTOR_INVESTMENT_AFTERMARKET_ACQUISITION: 14
    },
    userActivitiesSchema: mongoose.Schema({
        userFk: {type: String, index: true},
        activityType: Number,
        activityParameters: [{nameOfParameter: String, value: String}],
        timestamp: Date
    })
};