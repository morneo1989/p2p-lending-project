var mongoose = require("mongoose");

module.exports = {
    investorSharesCollectionName: "investorshares",
    investorSharesSchema: mongoose.Schema({
        amountOfInvestment: Number,
        investmentConfirmed: Boolean,
        investorFk: {type: String, index: true},
        auctionFk: {type: String, index: true},
        investmentAtAfterMarket: Boolean,
        timestamp: Date
    })
};