var nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
    host: 'mail.pospo.pl',
    port: 587,
    secure: false,
    requireTLS: true,
    tls: {
        rejectUnauthorized: false
    },
    //proxy: 'http://fwwar.gmv.es:80/',
    auth: {
        user: "m.szumielewicz@pospo.pl",
        pass: "wujekstaszek"
    }
});

var mailOptions = {
    from: 'm.szumielewicz@pospo.pl'
};

module.exports = {
    sendVerificationEmail: function (emailAddress, verificationCode, callback) {
        mailOptions.to = emailAddress;
        mailOptions.subject = "POSPO: Potwierdzenie rejestracji";
        var verificationUrl = "http://52.29.74.108:9080/users/register/email/verification/" + verificationCode;
        mailOptions.html = '<h2>Dziekujemy za zalozenie konta na platformie POSPO</h2>' +
            '<p>Aby dokonczyc rejestracje prosimy o potwierdzenie adresu email poprzez klikniecie w ponizszy adres:</p>' +
            '<p><a href="' + verificationUrl + '">Aktywacja konta</a></p>' +
            '<p>Po kliknieciu powyzszy link zostaniesz automatycznie zweryfikowany w systemie POSPO</p>';
        transporter.sendMail(mailOptions, callback);
    },
    sendPasswordResetEmail: function (emailAddress, resetCode, callback) {
        mailOptions.to = emailAddress;
        mailOptions.subject = "POSPO: Zmiana hasła";
        var passwordResetUrl = "http://52.29.74.108:9080/users/password/remember/" + resetCode;
        mailOptions.html = '<h2>Zmiana hasła w systemie POSPO</h2>' +
            '<p>Aby ustalić nowe hasło należy przejść pod podany poniżej adres:</p>' +
            '<p><a href="' + passwordResetUrl + '">Aktywacja konta</a></p>' +
            '<p>Po kliknieciu powyzszy link zostaniesz poproszony o podanie nowego hasła do systemu POSPO</p>';
        transporter.sendMail(mailOptions, callback);
    }
};