var moment = require("moment");
var winston = require("winston");
var winstonWithMongoDB = require("winston-mongodb");

var winstonDatabaseLogger = new winston.Logger({
    levels: {
        verbose: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4,
        fatal: 5
    },
    transports: [
        new winston.transports.MongoDB({
            db: 'mongodb://localhost:27017/p2pLendingDB',
            collection: 'systemLogs',
            capped: true,
            cappedMax: 10000,
            tryReconnect: true
        })
    ]
});

module.exports = {
    logLevels: {
        VERBOSE: "VERBOSE",
        DEBUG: "DEBUG",
        INFO: "INFO",
        WARN: "WARN",
        ERROR: "ERROR",
        FATAL: "FATAL"
    },
    enabledLogLevel: "INFO",
    setEnabledLogLevel: function(logLevel) {
        this.enabledLogLevel = logLevel;
    },
    logVerbose: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "VERBOSE") {
            winstonDatabaseLogger.verbose("[" + logMessage + "]", {module: moduleName});
        }
    },
    logDebug: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "DEBUG") {
            winstonDatabaseLogger.debug("[" + logMessage + "]", {module: moduleName});
        }
    },
    logInfo: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "INFO") {
            winstonDatabaseLogger.info("[" + logMessage + "]", {module: moduleName});
        }
    },
    logWarn: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "WARN") {
            winstonDatabaseLogger.warn("[" + logMessage + "]", {module: moduleName});
        }
    },
    logError: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "ERROR") {
            winstonDatabaseLogger.error("[" + logMessage + "]", {module: moduleName});
        }
    },
    logFatal: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "FATAL") {
            winstonDatabaseLogger.fatal("[" + logMessage + "]", {module: moduleName});
        }
    }
};