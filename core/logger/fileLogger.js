var moment = require("moment");
var winston = require("winston");

var winstonFileLogger = new winston.Logger({
    levels: {
        verbose: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4,
        fatal: 5
    },
    transports: [
        new winston.transports.File({ filename: 'logs/server.log' })
    ]
});

module.exports = {
    logLevels: {
        VERBOSE: "VERBOSE",
        DEBUG: "DEBUG",
        INFO: "INFO",
        WARN: "WARN",
        ERROR: "ERROR",
        FATAL: "FATAL"
    },
    enabledLogLevel: "INFO",
    setEnabledLogLevel: function(logLevel) {
        this.enabledLogLevel = logLevel;
    },
    logVerbose: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "VERBOSE") {
            winstonFileLogger.verbose("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logDebug: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "DEBUG") {
            winstonFileLogger.debug("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logInfo: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "INFO") {
            winstonFileLogger.info("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logWarn: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "WARN") {
            winstonFileLogger.warn("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logError: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "ERROR") {
            winstonFileLogger.error("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logFatal: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "FATAL") {
            winstonFileLogger.fatal("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    }
};