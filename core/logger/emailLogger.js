var Mail = require("winston-mail").Mail;
var winston = require("winston");

var mailSendOptions = {
    to: "szumielewiczmaciejjacek@gmail.com",
    from: "m.szumielewicz@pospo.pl",
    host: "mail.pospo.pl",
    port: 587,
    tls: true,
    username: "m.szumielewicz@pospo.pl",
    password: "wujekstaszek",
    subject: "POSPO:"

};

var emailNotificationSender = new winston.Logger({
    levels: {
        info: 1
    },
    transports: [
        new Mail(mailSendOptions)
    ]
});

module.exports = {
    send: function(subject, notificationMessage) {
        mailSendOptions.subject = subject;
        emailNotificationSender.info(notificationMessage);
    }
};