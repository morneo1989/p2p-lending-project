var moment = require("moment");
var winston = require("winston");

var logger = new winston.Logger({
    transports: [
        new winston.transports.Console()
    ]
});

var consoleLogger = new (winston.Logger)({
    levels: {
        verbose: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4,
        fatal: 5
    },
    transports: [
        new (winston.transports.Console)()
    ]
});

module.exports = {
    logLevels: {
        VERBOSE: "VERBOSE",
        DEBUG: "DEBUG",
        INFO: "INFO",
        WARN: "WARN",
        ERROR: "ERROR",
        FATAL: "FATAL"
    },
    enabledLogLevel: "INFO",
    setEnabledLogLevel: function(logLevel) {
        this.enabledLogLevel = logLevel;
    },
    logVerbose: function(moduleName, logMessage) {
        if(this.enabledLogLevel === "FATAL" && this.enabledLogLevel !== "ERROR" && this.enabledLogLevel !== "WARN" && this.enabledLogLevel !== "INFO" && this.enabledLogLevel !== "DEBUG") {
            consoleLogger.verbose("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logDebug: function(moduleName, logMessage) {
        if(this.enabledLogLevel !== "FATAL" && this.enabledLogLevel !== "ERROR" && this.enabledLogLevel !== "WARN" && this.enabledLogLevel !== "INFO") {
            consoleLogger.debug("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logInfo: function(moduleName, logMessage) {
        if(this.enabledLogLevel !== "FATAL" && this.enabledLogLevel !== "ERROR" && this.enabledLogLevel !== "WARN") {
            logger.info("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logWarn: function(moduleName, logMessage) {
        if(this.enabledLogLevel !== "FATAL" && this.enabledLogLevel !== "ERROR") {
            logger.warn("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logError: function(moduleName, logMessage) {
        if(this.enabledLogLevel !== "FATAL") {
            logger.error("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
        }
    },
    logFatal: function(moduleName, logMessage) {
            logger.fatal("[" + logMessage + "]", {timestamp: moment().format(), module: moduleName});
    }
};