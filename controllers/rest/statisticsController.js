var requestLogger = require("./requestsLogger");
var apiSecurityService = require("../security/apiSecurityService");
var consoleLogger = require("../../core/logger/consoleLogger");
var systemStatisticsContent = require("../../db/model/administration/systemStatistics");
var moment = require("moment");

module.exports = {

    init: function (app, statisticsDao) {
        /* Auctions */
        app.get('/statistics/auctions', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (request, response) {
            statisticsDao.findAllOrderedDesc(function (isError, results) {
                if (isError) {
                    consoleLogger.logError("statisticsController", "Error while getting statistics from DB: " + isError);
                    response.status(403).end();
                } else if (results == null || results.length == 0) {
                    consoleLogger.logDebug("statisticsController", "No statistics at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("statisticsController", "Found statistics at DB");
                    response.status(200).json(results[0]).end();
                }
            });
        });
    }
};