var requestLogger = require("../requestsLogger");
var apiSecurityService = require("../../security/apiSecurityService");
var consoleLogger = require("../../../core/logger/consoleLogger");
var auctionsModel = require("../../../db/model/main/auctions");
var moment = require("moment");

module.exports = {

    validateNewApplicationInput: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (checkIfValueNonValid(request.body.title) || checkIfValueNonValid(request.body.description)
            || checkIfValueNonValid(request.body.loanTarget) || checkIfValueNonValid(request.body.loanAmount)
            || checkIfValueNonValid(request.body.rateOfLoan) || checkIfValueNonValid(request.body.payoffPeriodInMonths)) {
            consoleLogger.logError("auctionsController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },

    init: function (app, usersDao, auctionsDao, investmentsDao) {
        app.post('/auctions/application', requestLogger.logRequest, this.validateNewApplicationInput, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email}, function (isError, result) {
                if (isError) {
                    consoleLogger.logError("auctionsController", "No user at DB with email: " + req.email);
                    res.status(403).end();
                } else if (result.length != 1) {
                    consoleLogger.logError("auctionsController", "More then one user with email: " + req.email);
                    res.status(500).end();
                } else if (result.length == 0) {
                    consoleLogger.logError("auctionsController", "No user with email: " + req.email);
                    res.status(500).end();
                } else {
                    var user = result[0];
                    var newAuction = auctionsModel.createAuction();
                    newAuction.title = req.body.title;
                    newAuction.description = req.body.description;
                    newAuction.loanAmount = req.body.loanAmount;
                    newAuction.loanTarget = req.body.loanTarget;
                    newAuction.rateOfLoan = req.body.rateOfLoan;
                    newAuction.timestampOfCreation = moment();
                    newAuction.borrowerFk = user._id.toString();
                    newAuction.borrowerApplication.claimedAmount = req.body.loanAmount;
                    newAuction.borrowerApplication.claimedPayoffPeriodInMonths = req.body.payoffPeriodInMonths;
                    newAuction.borrowerApplication.claimedRateOfLoan = req.body.rateOfLoan;
                    newAuction.borrowerApplication.timestamp = moment();
                    auctionsDao.save(newAuction, function (isError) {
                        if (isError) {
                            consoleLogger.logError("auctionsController", "Cannot save new auction: " + isError);
                            res.status(500).end();
                        } else {
                            consoleLogger.logInfo("auctionsController", "Saved new auction for user with email: " + req.email);
                            res.status(201).end();
                        }
                    });
                }
            });
        });
        app.get('/auctions', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email},
                function (isError, results) {
                    if (isError) {
                        consoleLogger.logError("auctionsController", "No user at DB with email: " + req.email);
                        res.status(403).end();
                    } else if (results.length != 1) {
                        consoleLogger.logError("auctionsController", "More then one user with email: " + req.email);
                        res.status(500).end();
                    } else if (results.length == 0) {
                        consoleLogger.logError("auctionsController", "No user with email: " + req.email);
                        res.status(500).end();
                    } else {
                        var user = results[0];
                        auctionsDao.findAll(function (isError, results) {
                            if (results == null || results.length == 0) {
                                res.status(404).end();
                            } else {
                                res.status(200).json(results).end()
                            }
                        })
                    }
                });
        });
        app.get('/auctions/investor', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email},
                function (isError, results) {
                    if (isError) {
                        consoleLogger.logError("auctionsController", "No user at DB with email: " + req.email);
                        res.status(403).end();
                    } else if (results.length > 1) {
                        consoleLogger.logError("auctionsController", "More then one user with email: " + req.email);
                        res.status(500).end();
                    } else if (results.length == 0) {
                        consoleLogger.logError("auctionsController", "No user with email: " + req.email);
                        res.status(500).end();
                    } else {
                        var user = results[0];
                        investmentsDao.findBy({investorFk: user._id},
                            function (isError, results) {
                                if (isError) {
                                    consoleLogger.logError("auctionsController", "No investments at DB with email: " + req.email);
                                    res.status(403).end();
                                } else if (results.length == 0) {
                                    consoleLogger.logError("auctionsController", "No investments with email: " + req.email);
                                    res.status(500).end();
                                } else {
                                    consoleLogger.logInfo("auctionsController", JSON.stringify(results));
                                    var checkIfAuctionInInvestments = function(auction, investments) {
                                        for(var index = 0; index < investments.length; index++) {
                                            if(investments[index].auctionFk == auction._id) {
                                                return true;
                                            }
                                        }
                                        return false;
                                    };
                                    var auctionsToReturn = [results.length];
                                    var indexToInsert = 0;
                                    auctionsDao.findAll(function (isError, auctions) {
                                        if (auctions == null || auctions.length == 0) {
                                            res.status(404).end();
                                        } else {
                                            consoleLogger.logInfo("auctionsController", JSON.stringify(auctions));
                                            for(var index = 0;index < auctions.length;index++) {
                                                if(checkIfAuctionInInvestments(auctions[index], results)) {
                                                    auctionsToReturn[indexToInsert] = auctions[index];
                                                    indexToInsert++;
                                                }
                                            }
                                            res.status(200).json(auctionsToReturn).end();
                                        }
                                    })
                                }
                            });
                    }
                });
        });
        app.get('/auctions/borrower', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email},
                function (isError, results) {
                    if (isError) {
                        consoleLogger.logError("auctionsController", "No user at DB with email: " + req.email);
                        res.status(403).end();
                    } else if (results.length != 1) {
                        consoleLogger.logError("auctionsController", "More then one user with email: " + req.email);
                        res.status(500).end();
                    } else if (results.length == 0) {
                        consoleLogger.logError("auctionsController", "No user with email: " + req.email);
                        res.status(500).end();
                    } else {
                        var user = results[0];
                        auctionsDao.findBy({borrowerFk: user._id.toString()}, function (isError, results) {
                            if (results == null || results.length == 0) {
                                res.status(404).end();
                            } else {
                                res.status(200).json(results).end()
                            }
                        })
                    }
                });
        });
        app.get('/auctions/admin', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email},
                function (isError, results) {
                    if (isError) {
                        consoleLogger.logError("auctionsController", "No user at DB with email: " + req.email);
                        res.status(403).end();
                    } else if (results.length != 1) {
                        consoleLogger.logError("auctionsController", "More then one user with email: " + req.email);
                        res.status(500).end();
                    } else if (results.length == 0) {
                        consoleLogger.logError("auctionsController", "No user with email: " + req.email);
                        res.status(500).end();
                    } else {
                        var user = results[0];
                        auctionsDao.findAll(function (isError, results) {
                            if (results == null || results.length == 0) {
                                res.status(404).end();
                            } else {
                                res.status(200).json(results).end()
                            }
                        })
                    }
                });
        });
    }
};