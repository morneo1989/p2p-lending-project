var requestLogger = require("../requestsLogger");
var apiSecurityService = require("../../security/apiSecurityService");
var consoleLogger = require("../../../core/logger/consoleLogger");
var investmentsModel = require("../../../db/model/main/investorShares");
var moment = require("moment");

module.exports = {

    init: function (app, usersDao, investmentsDao) {
        app.post('/investments', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email}, function (isError, result) {
                if (isError) {
                    consoleLogger.logError("investmentsController", "No user at DB with email: " + req.email);
                    res.status(403).end();
                } else if (result.length > 1) {
                    consoleLogger.logError("investmentsController", "More then one user with email: " + req.email);
                    res.status(500).end();
                } else if (result.length == 0) {
                    consoleLogger.logError("investmentsController", "No user with email: " + req.email);
                    res.status(500).end();
                } else {
                    var user = result[0];
                    var newInvestment = investmentsModel.createInvestorShare();
                    newInvestment.amountOfInvestment = req.body.amountOfInvestment;
                    newInvestment.auctionFk = req.body.auctionFk;
                    newInvestment.investmentAtAfterMarket = false;
                    newInvestment.investmentConfirmed = false;
                    newInvestment.investorFk = user._id;
                    newInvestment.timestamp = moment();
                    investmentsDao.save(newInvestment, function (isError) {
                        if (isError) {
                            consoleLogger.logError("investmentsController", "Cannot save new investment: " + isError);
                            res.status(500).end();
                        } else {
                            consoleLogger.logInfo("investmentsController", "Saved new investment for user with email: " + req.email);
                            res.status(201).end();
                        }
                    });
                }
            });
        });
        app.get('/investments', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (req, res) {
            usersDao.findBy({email: req.email}, function (isError, result) {
                if (isError) {
                    consoleLogger.logError("investmentsController", "No user at DB with email: " + req.email);
                    res.status(403).end();
                } else if (result.length > 1) {
                    consoleLogger.logError("investmentsController", "More then one user with email: " + req.email);
                    res.status(500).end();
                } else if (result.length == 0) {
                    consoleLogger.logError("investmentsController", "No user with email: " + req.email);
                    res.status(500).end();
                } else {
                    var user = result[0];
                    investmentsDao.findBy({investorFk: user._id}, function (isError, results) {
                        if (isError) {
                            consoleLogger.logError("investmentsController", "Cannot get investments: " + isError);
                            res.status(500).end();
                        } else if(results == null || results.length == 0) {
                            res.status(404).end();
                        } else {
                            res.status(200).json(results).end();
                        }
                    });
                }
            });
        });
    }
};