var requestLogger = require("../requestsLogger");
var apiSecurityService = require("../../security/apiSecurityService");
var consoleLogger = require("../../../core/logger/consoleLogger");
var moderationsContent = require("../../../db/model/administration/moderationContents");
var moment = require("moment");

module.exports = {

    validateLegalRegulation: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (checkIfValueNonValid(request.body.legalRegulationTitle) || checkIfValueNonValid(request.body.htmlFormattedText)) {
            consoleLogger.logError("moderationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },
    validateFAQ: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        for(var index = 0; index < request.body.length; index++) {
            if (checkIfValueNonValid(request.body[index].faqQuestion) || checkIfValueNonValid(request.body[index].htmlFormattedText)) {
                consoleLogger.logError("moderationController", request.method + " - " + request.path + ": Invalid input for request");
                response.status(400).end();
                return;
            }
        }
        next();
    },
    validateModerationContent: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (checkIfValueNonValid(checkIfValueNonValid(request.body.htmlFormattedText))) {
            consoleLogger.logError("moderationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },

    init: function (app, moderationContentDao) {
        /* Legal regulations */
        app.post('/moderation/legalRegulations', requestLogger.logRequest, this.validateLegalRegulation, apiSecurityService.authenticateRequest, function (request, response) {
            if(request.body._id != undefined && request.body._id != null) {
                consoleLogger.logInfo('Updating legal regulation with id: ' + request.body._id);
                moderationContentDao.updateById(request.body._id, {htmlFormattedText: request.body.htmlFormattedText}, function (isError, numAffected) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot update legal regulation: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                        if(numAffected.nModified == 0) {
                            consoleLogger.logDebug("moderationController", "No legal regulation updated");
                            response.status(500).end();
                        } else if(numAffected.nModified == 1) {
                            consoleLogger.logDebug("moderationController", "Updated legal regulation with title: " + request.body.title);
                            response.status(200).end();
                        }
                    }
                });
            } else {
                consoleLogger.logInfo('Creating new legal regulation with title: ' + request.body.title);
                var legalRegulation = moderationsContent.createLegalRegulation();
                legalRegulation.contentName = 'legalRegulation';
                legalRegulation.legalRegulationTitle = request.body.legalRegulationTitle;
                legalRegulation.htmlFormattedText = request.body.htmlFormattedText;
                moderationContentDao.save(legalRegulation, function (isError) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot save legal regulation: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logInfo("moderationController", "Saved legal regulation with title: " + request.body.title);
                        response.status(201).end();
                    }

                });
            }

        });
        app.get('/moderation/legalRegulations/titles', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findAllLegalRegulationsWithIdAndTitle(function (isError, results) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting legal regulations from DB: " + isError);
                    response.status(403).end();
                } else if (results == null || results.length == 0) {
                    consoleLogger.logDebug("moderationController", "No legal regulations at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found legal regulations at DB");
                    response.status(200).json(results).end();
                }
            });
        });
        app.get('/moderation/legalRegulations/:regulationId', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findById(request.params.regulationId, function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting legal regulations from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logError("moderationController", "No legal regulations at DB");
                    response.status(500).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found legal regulation at DB");
                    response.status(200).json(result[0]).end();
                }
            });
        });
        /* FAQ */
        app.get('/moderation/faq', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findAllByContentName("FAQ", function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting FAQ content from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logDebug("moderationController", "No FAQ content at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found FAQ at DB");
                    response.status(200).json(result).end();
                }
            });
        });
        app.post('/moderation/faq', requestLogger.logRequest, this.validateFAQ, apiSecurityService.authenticateRequest, function (request, response) {
            for(var index = 0; index < request.body.length; index++) {
                if(request.body[index]._id != undefined && request.body[index]._id != null) {
                    consoleLogger.logInfo('Updating FAQ with id: ' + request.body[index]._id);
                    moderationContentDao.updateById(request.body[index]._id, {faqQuestion: request.body[index].faqQuestion, htmlFormattedText: request.body[index].htmlFormattedText},
                        function (isError, numAffected) {
                        if (isError) {
                            consoleLogger.logError("moderationController", "Cannot update FAQ: " + isError);
                            response.status(500).end();
                        } else {
                            consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                            if(numAffected.nModified == 0) {
                                consoleLogger.logDebug("moderationController", "No FAQ updated");
                                response.status(500).end();
                            } else if(numAffected.nModified == 1) {
                                consoleLogger.logDebug("moderationController", "Updated FAQ");
                                response.status(200).end();
                            }
                        }
                    });
                } else {
                    consoleLogger.logInfo('Creating new FAQ content');
                    var faq = moderationsContent.createFaq();
                    faq.htmlFormattedText = request.body[index].htmlFormattedText;
                    faq.faqQuestion = request.body[index].faqQuestion;
                    moderationContentDao.save(faq, function (isError) {
                        if (isError) {
                            consoleLogger.logError("moderationController", "Cannot save FAQ content: " + isError);
                            response.status(500).end();
                        } else {
                            consoleLogger.logInfo("moderationController", "Saved FAQ content");
                            response.status(201).end();
                        }

                    });
                }
            }
        });
        /* P2P About */
        app.get('/moderation/p2pAbout', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findByContentName('P2P_About', function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting P2P_About content from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logDebug("moderationController", "No P2P_About content at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found P2P_About at DB");
                    response.status(200).json(result[0]).end();
                }
            });
        });
        app.post('/moderation/p2pAbout', requestLogger.logRequest, this.validateModerationContent, apiSecurityService.authenticateRequest, function (request, response) {
            if(request.body._id != undefined && request.body._id != null) {
                consoleLogger.logInfo('Updating P2P_About with id: ' + request.body._id);
                moderationContentDao.updateById(request.body._id, {htmlFormattedText: request.body.htmlFormattedText}, function (isError, numAffected) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot update P2P_About: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                        if(numAffected.nModified == 0) {
                            consoleLogger.logDebug("moderationController", "No P2P_About updated");
                            response.status(500).end();
                        } else if(numAffected.nModified == 1) {
                            consoleLogger.logDebug("moderationController", "Updated P2P_About");
                            response.status(200).end();
                        }
                    }
                });
            } else {
                consoleLogger.logInfo('Creating new P2P_About content');
                var p2pAbout = moderationsContent.createP2PAbout();
                p2pAbout.htmlFormattedText = request.body.htmlFormattedText;
                moderationContentDao.save(p2pAbout, function (isError) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot save P2P_About content: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logInfo("moderationController", "Saved P2P_About content");
                        response.status(201).end();
                    }

                });
            }
        });
        /* Invest Guide */
        app.get('/moderation/investGuide', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findByContentName('Invest_Guide', function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting Invest_Guide content from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logDebug("moderationController", "No Invest_Guide content at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found Invest_Guide at DB");
                    response.status(200).json(result[0]).end();
                }
            });
        });
        app.post('/moderation/investGuide', requestLogger.logRequest, this.validateModerationContent, apiSecurityService.authenticateRequest, function (request, response) {
            if(request.body._id != undefined && request.body._id != null) {
                consoleLogger.logInfo('Updating Invest Guide with id: ' + request.body._id);
                moderationContentDao.updateById(request.body._id, {htmlFormattedText: request.body.htmlFormattedText}, function (isError, numAffected) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot update Invest Guide: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                        if(numAffected.nModified == 0) {
                            consoleLogger.logDebug("moderationController", "No Invest Guide updated");
                            response.status(500).end();
                        } else if(numAffected.nModified == 1) {
                            consoleLogger.logDebug("moderationController", "Updated Invest Guide");
                            response.status(200).end();
                        }
                    }
                });
            } else {
                consoleLogger.logInfo('Creating new Invest Guide content');
                var investGuide = moderationsContent.createInvestGuide();
                investGuide.htmlFormattedText = request.body.htmlFormattedText;
                moderationContentDao.save(investGuide, function (isError) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot save Invest Guide content: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logInfo("moderationController", "Saved Invest Guide content");
                        response.status(201).end();
                    }

                });
            }
        });
        /* Borrow Guide */
        app.get('/moderation/borrowGuide', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findByContentName('Borrow_Guide', function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting Borrow_Guide content from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logDebug("moderationController", "No Borrow_Guide content at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found Borrow_Guide at DB");
                    response.status(200).json(result[0]).end();
                }
            });
        });
        app.post('/moderation/borrowGuide', requestLogger.logRequest, this.validateModerationContent, apiSecurityService.authenticateRequest, function (request, response) {
            if(request.body._id != undefined && request.body._id != null) {
                consoleLogger.logInfo('Updating Borrow Guide with id: ' + request.body._id);
                moderationContentDao.updateById(request.body._id, {htmlFormattedText: request.body.htmlFormattedText}, function (isError, numAffected) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot update Borrow Guide: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                        if(numAffected.nModified == 0) {
                            consoleLogger.logDebug("moderationController", "No Borrow Guide updated");
                            response.status(500).end();
                        } else if(numAffected.nModified == 1) {
                            consoleLogger.logDebug("moderationController", "Updated Borrow Guide");
                            response.status(200).end();
                        }
                    }
                });
            } else {
                consoleLogger.logInfo('Creating new Borrow Guide content');
                var borrowGuide = moderationsContent.createBorrowGuide();
                borrowGuide.htmlFormattedText = request.body.htmlFormattedText;
                moderationContentDao.save(borrowGuide, function (isError) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot save Borrow Guide content: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logInfo("moderationController", "Saved Borrow Guide content");
                        response.status(201).end();
                    }

                });
            }
        });
        /* Contact */
        app.get('/moderation/contact', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findByContentName('Contact', function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting Contact content from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logDebug("moderationController", "No Contact content at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found Contact at DB");
                    response.status(200).json(result[0]).end();
                }
            });
        });
        app.post('/moderation/contact', requestLogger.logRequest, this.validateModerationContent, apiSecurityService.authenticateRequest, function (request, response) {
            if(request.body._id != undefined && request.body._id != null) {
                consoleLogger.logInfo('Updating Contact with id: ' + request.body._id);
                moderationContentDao.updateById(request.body._id, {htmlFormattedText: request.body.htmlFormattedText}, function (isError, numAffected) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot update Contact: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                        if(numAffected.nModified == 0) {
                            consoleLogger.logDebug("moderationController", "No Contact updated");
                            response.status(500).end();
                        } else if(numAffected.nModified == 1) {
                            consoleLogger.logDebug("moderationController", "Updated Contact");
                            response.status(200).end();
                        }
                    }
                });
            } else {
                consoleLogger.logInfo('Creating new Contact content');
                var contact = moderationsContent.createContact();
                contact.htmlFormattedText = request.body.htmlFormattedText;
                moderationContentDao.save(contact, function (isError) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot save Contact content: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logInfo("moderationController", "Saved Contact content");
                        response.status(201).end();
                    }

                });
            }
        });
        /* About Us */
        app.get('/moderation/aboutUs', requestLogger.logRequest, function (request, response) {
            moderationContentDao.findByContentName('About_Us', function (isError, result) {
                if (isError) {
                    consoleLogger.logError("moderationController", "Error while getting About_Us content from DB: " + isError);
                    response.status(403).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logDebug("moderationController", "No About_Us content at DB");
                    response.status(404).end();
                } else {
                    consoleLogger.logInfo("moderationController", "Found About_Us at DB");
                    response.status(200).json(result[0]).end();
                }
            });
        });
        app.post('/moderation/aboutUs', requestLogger.logRequest, this.validateModerationContent, apiSecurityService.authenticateRequest, function (request, response) {
            if(request.body._id != undefined && request.body._id != null) {
                consoleLogger.logInfo('Updating About_Us with id: ' + request.body._id);
                moderationContentDao.updateById(request.body._id, {htmlFormattedText: request.body.htmlFormattedText}, function (isError, numAffected) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot update About_Us: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logDebug("moderationController", "numAffected: " + JSON.stringify(numAffected));
                        if(numAffected.nModified == 0) {
                            consoleLogger.logDebug("moderationController", "No About_Us updated");
                            response.status(500).end();
                        } else if(numAffected.nModified == 1) {
                            consoleLogger.logDebug("moderationController", "Updated About_Us");
                            response.status(200).end();
                        }
                    }
                });
            } else {
                consoleLogger.logInfo('Creating new About_Us content');
                var aboutUs = moderationsContent.createAboutUs();
                aboutUs.htmlFormattedText = request.body.htmlFormattedText;
                moderationContentDao.save(aboutUs, function (isError) {
                    if (isError) {
                        consoleLogger.logError("moderationController", "Cannot save About_Us content: " + isError);
                        response.status(500).end();
                    } else {
                        consoleLogger.logInfo("moderationController", "Saved About_Us content");
                        response.status(201).end();
                    }

                });
            }
        });
    }
};