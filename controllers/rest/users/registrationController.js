var usersModel = require("../../../db/model/main/users");
var phoneVerificationModel = require("../../../db/model/main/phoneVerifications");
var consoleLogger = require("../../../core/logger/consoleLogger");
var emailSendService = require("../../../core/email/emailSendService");
var uuidGenerator = require("node-uuid");
var moment = require("moment");
var requestLogger = require("../requestsLogger");
var auctionsModel = require("../../../db/model/main/auctions");

module.exports = {

    validatePhoneValidationInput: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (checkIfValueNonValid(request.body.email) || checkIfValueNonValid(request.body.phone)) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },
    validateNewInvestorSimpleInput: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (checkIfValueNonValid(request.body.phoneVerificationCode) || checkIfValueNonValid(request.body.email) || checkIfValueNonValid(request.body.name)
            || checkIfValueNonValid(request.body.surname) || checkIfValueNonValid(request.body.password)
            || checkIfValueNonValid(request.body.phone)) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },
    validateNewInvestorFullInput: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (checkIfValueNonValid(request.body.phoneVerificationCode) || checkIfValueNonValid(request.body.representingPerson.email)
            || checkIfValueNonValid(request.body.representingPerson.name)
            || checkIfValueNonValid(request.body.representingPerson.surname) || checkIfValueNonValid(request.body.representingPerson.password)
            || checkIfValueNonValid(request.body.representingPerson.phone)
            || checkIfValueNonValid(request.body.company.name) || checkIfValueNonValid(request.body.company.form)
            || checkIfValueNonValid(request.body.company.taxId)
            || checkIfValueNonValid(request.body.company.registeredAddress.city) || checkIfValueNonValid(request.body.company.registeredAddress.flatNumber)
            || checkIfValueNonValid(request.body.company.registeredAddress.houseNumber) || checkIfValueNonValid(request.body.company.registeredAddress.postalCode)
            || checkIfValueNonValid(request.body.company.registeredAddress.street)
            || checkIfValueNonValid(request.body.company.correspondenceAddress.city) || checkIfValueNonValid(request.body.company.correspondenceAddress.flatNumber)
            || checkIfValueNonValid(request.body.company.correspondenceAddress.houseNumber) || checkIfValueNonValid(request.body.company.correspondenceAddress.postalCode)
            || checkIfValueNonValid(request.body.company.correspondenceAddress.street)) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },
    validateNewBorrowerSimpleInput: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (!checkIfValueNonValid(request.body.loanApplication)) {
            if (checkIfValueNonValid('description', request.body.loanApplication.description) || checkIfValueNonValid('loanTarget', request.body.loanApplication.loanTarget)
                || checkIfValueNonValid('loanAmount', request.body.loanApplication.loanAmount) || checkIfValueNonValid('rateOfLoan', request.body.loanApplication.rateOfLoan)
                || checkIfValueNonValid('payoffPeriodInMonths', request.body.loanApplication.payoffPeriodInMonths)) {
                consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
                response.status(400).end();
            }
        }
        if (checkIfValueNonValid(request.body.phoneVerificationCode) || checkIfValueNonValid(request.body.email) || checkIfValueNonValid(request.body.name)
            || checkIfValueNonValid(request.body.surname) || checkIfValueNonValid(request.body.password)
            || checkIfValueNonValid(request.body.phone)) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },
    validateNewBorrowerFullInput: function (request, response, next) {
        var checkIfValueNonValid = function (value) {
            return (value === null || value === undefined);
        };
        if (!checkIfValueNonValid(request.body.loanApplication)) {
            if (checkIfValueNonValid('description', request.body.loanApplication.description) || checkIfValueNonValid('loanTarget', request.body.loanApplication.loanTarget)
                || checkIfValueNonValid('loanAmount', request.body.loanApplication.loanAmount) || checkIfValueNonValid('rateOfLoan', request.body.loanApplication.rateOfLoan)
                || checkIfValueNonValid('payoffPeriodInMonths', request.body.loanApplication.payoffPeriodInMonths)) {
                consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
                response.status(400).end();
            }
        }
        if (checkIfValueNonValid(request.body.phoneVerificationCode) || checkIfValueNonValid(request.body.email) || checkIfValueNonValid(request.body.name)
            || checkIfValueNonValid(request.body.surname) || checkIfValueNonValid(request.body.password)
            || checkIfValueNonValid(request.body.userType) || checkIfValueNonValid(request.body.phone)
            || checkIfValueNonValid(request.body.registeredAddress.city) || checkIfValueNonValid(request.body.registeredAddress.flatNumber)
            || checkIfValueNonValid(request.body.registeredAddress.houseNumber) || checkIfValueNonValid(request.body.registeredAddress.postalCode)
            || checkIfValueNonValid(request.body.registeredAddress.street)
            || checkIfValueNonValid(request.body.correspondenceAddress.city) || checkIfValueNonValid(request.body.correspondenceAddress.flatNumber)
            || checkIfValueNonValid(request.body.correspondenceAddress.houseNumber) || checkIfValueNonValid(request.body.correspondenceAddress.postalCode)
            || checkIfValueNonValid(request.body.correspondenceAddress.street)
            || checkIfValueNonValid(request.body.ratingParameters.age) || checkIfValueNonValid(request.body.ratingParameters.currentDebt)
            || checkIfValueNonValid(request.body.ratingParameters.employmentArea) || checkIfValueNonValid(request.body.ratingParameters.incomeSource)
            || checkIfValueNonValid(request.body.ratingParameters.maritalStatus) || checkIfValueNonValid(request.body.ratingParameters.maritalStatusPeriodInMonths)
            || checkIfValueNonValid(request.body.ratingParameters.monthlyAverageIncome) || checkIfValueNonValid(request.body.ratingParameters.numberOfPeopleDependent)) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },
    validateEmailVerificationInput: function (req, res, next) {
        if (req.params.verificationCode == null || req.params.verificationCode == undefined) {
            consoleLogger.logError("registrationController", req.method + " - " + req.path + ": Invalid input for request");
            res.status(400).end();
        } else {
            next();
        }
    },

    init: function (app, usersDao, auctionsDao, phoneVerificationsDao) {
        app.get('/users/register/email/verification/:verificationCode', requestLogger.logRequest, this.validateEmailVerificationInput, function (request, response) {
            var verificationCode = request.params.verificationCode;
            usersDao.findSelectedBy("email emailVerification.isVerified emailVerification.code emailVerification.verificationSentTimestamp",
                {'emailVerification.code': verificationCode}, function (isError, result) {
                    if (isError) {
                        consoleLogger.logError("registrationController", "Error while finding new user by email verification code: " + verificationCode + ": " + isError);
                        response.redirect('http://52.29.74.108:9080/pospo/new-user?response=error');
                    } else if (result == null || result.length == 0) {
                        consoleLogger.logError("registrationController", "No user found by email verification code:" + verificationCode);
                        response.redirect('http://52.29.74.108:9080/pospo/new-user?response=error');
                    } else {
                        var registeredUser = result[0];
                        if (registeredUser.emailVerification.isVerified) {
                            consoleLogger.logDebug("registrationController", "User's email already verified");
                            response.redirect('http://52.29.74.108:9080/pospo/new-user?response=ok');
                        } else {
                            usersDao.updateBy({email: registeredUser.email}, {
                                    emailVerification: {
                                        isVerified: true,
                                        code: registeredUser.emailVerification.code,
                                        verificationSentTimestamp: registeredUser.emailVerification.verificationSentTimestamp
                                    }
                                },
                                function (isError, numAffected) {
                                    if (isError) {
                                        consoleLogger.logError("registrationController", "Error while updating user to verified: " + isError);
                                        response.redirect('http://52.29.74.108:9080/pospo/new-user?response=error');
                                    } else if (numAffected.nModified != 1) {
                                        consoleLogger.logError("registrationController", "Cannot verify email of user with email: " + registeredUser.email);
                                        response.redirect('http://52.29.74.108:9080/pospo/new-user?response=error');
                                    } else {
                                        consoleLogger.logDebug("registrationController", "Verified user email");
                                        response.redirect('http://52.29.74.108:9080/pospo/new-user?response=ok');
                                    }
                                });
                        }
                    }
                });
        });
        app.post('/users/register/phone/verification', requestLogger.logRequest, this.validatePhoneValidationInput, function (request, response) {
            usersDao.findSelectedBy("_id name",
                {'email': request.body.email}, function (isError, result) {
                    if (isError) {
                        consoleLogger.logError("registrationController", "Error while finding user by email: " + request.body.email + ": " + isError);
                        response.status(500).end();
                    } else if (result != null && result.length > 0) {
                        consoleLogger.logError("registrationController", "User with email " + request.body.email + " already exists at DB");
                        response.status(409).end();
                    } else {
                        var usersPhoneToVerify = phoneVerificationModel.createPhoneVerification();
                        usersPhoneToVerify.email = request.body.email;
                        usersPhoneToVerify.phone = request.body.phone;
                        usersPhoneToVerify.phoneVerificationCode = 111111;
                        phoneVerificationsDao.save(usersPhoneToVerify, function (isError) {
                            if (isError) {
                                consoleLogger.logError("registrationController", "Error while saving user's phone to verify: " + isError);
                                response.status(500).end();
                            } else {
                                consoleLogger.logDebug("registrationController", "Saved phone to validate for user with email: " + request.body.email);
                                response.status(200).end();
                            }
                        });
                    }
                });
        });
        app.post('/users/register/basic/investor', requestLogger.logRequest, this.validateNewInvestorSimpleInput, function (request, response) {
            usersDao.findSelectedBy("_id name", {email: request.body.email}, function (isError, results) {
                if (isError) {
                    consoleLogger.logError("registrationController", "Error while finding user by email: " + isError);
                    response.status(500).end();
                } else {
                    if (results != null && results.length > 0) {
                        consoleLogger.logWarn("registrationController", "User with email " + request.body.email + " already exists in DB");
                        response.status(409).end();
                    } else {
                        phoneVerificationsDao.findBy({
                            email: request.body.email,
                            phoneVerificationCode: request.body.phoneVerificationCode
                        }, function (isError, results) {
                            if (results == null || results.length == 0) {
                                consoleLogger.logWarn("registrationController", "No phone verification for email " + request.body.email + " and given verification code " + request.body.phoneVerificationCode + " at DataBase");
                                response.json({response: "PHONE_VERIFICATION_CODE_INVALID"}).status(403).end();
                            } else {
                                var userToRegister = usersModel.createUser();
                                userToRegister.email = request.body.email;
                                userToRegister.password = usersModel.hashPassword(request.body.password);
                                userToRegister.role = usersModel.userRoles.INVESTOR;
                                userToRegister.name = request.body.name;
                                userToRegister.surname = request.body.surname;
                                userToRegister.phone = results[0].phoneNumber;
                                userToRegister.timestampOfCreation = moment();
                                userToRegister.emailVerification.isVerified = false;
                                userToRegister.emailVerification.code = uuidGenerator.v1();
                                userToRegister.representingPerson.name = request.body.name;
                                userToRegister.representingPerson.surname = request.body.surname;
                                usersDao.save(userToRegister, function (isError) {
                                    if (isError) {
                                        consoleLogger.logError("registrationController", "Error while saving new user: " + isError);
                                        response.status(500).end();
                                    } else {
                                        emailSendService.sendVerificationEmail(userToRegister.email, userToRegister.emailVerification.code, function (error, info) {
                                            if (error) {
                                                consoleLogger.logError("registrationController", "Error while sending verification email: " + error + " : " + info);
                                                usersDao.removeBy({email: userToRegister.email}, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while removing user: " + isError);
                                                    }
                                                    response.status(500).end();
                                                });
                                            } else {
                                                consoleLogger.logInfo("registrationController", "Registered new user");
                                                usersDao.updateBy({'email': userToRegister.email}, {
                                                    emailVerification: {
                                                        isVerified: false,
                                                        code: userToRegister.emailVerification.code,
                                                        verificationSentTimestamp: moment()
                                                    }
                                                }, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while updating user: " + isError);
                                                        response.status(500).end();
                                                    } else {
                                                        response.status(201).end();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
        });
        app.post('/users/register/full/investor', requestLogger.logRequest, this.validateNewInvestorFullInput, function (request, response) {
            usersDao.findSelectedBy("_id name", {email: request.body.representingPerson.email}, function (isError, results) {
                if (isError) {
                    consoleLogger.logError("registrationController", "Error while finding user by email: " + isError);
                    response.status(500).end();
                } else {
                    if (results != null && results.length > 0) {
                        consoleLogger.logWarn("registrationController", "User with email " + request.body.representingPerson.email + " already exists in DB");
                        response.status(409).end();
                    } else {
                        phoneVerificationsDao.findBy({
                            email: request.body.representingPerson.email,
                            phoneVerificationCode: request.body.phoneVerificationCode
                        }, function (isError, results) {
                            if (results == null || results.length == 0) {
                                consoleLogger.logWarn("registrationController", "No phone verification for email " + request.body.email + " and given verification code " + request.body.phoneVerificationCode + " at DataBase");
                                response.json({response: "PHONE_VERIFICATION_CODE_INVALID"}).status(403).end();
                            } else {
                                var userToRegister = usersModel.createUser();
                                userToRegister.email = request.body.representingPerson.email;
                                userToRegister.password = usersModel.hashPassword(request.body.representingPerson.password);
                                userToRegister.role = usersModel.userRoles.INVESTOR;
                                userToRegister.name = request.body.representingPerson.name;
                                userToRegister.surname = request.body.representingPerson.surname;
                                userToRegister.phone = results[0].phoneNumber;
                                userToRegister.timestampOfCreation = moment();
                                userToRegister.emailVerification.isVerified = false;
                                userToRegister.emailVerification.code = uuidGenerator.v1();
                                userToRegister.representingPerson.name = request.body.representingPerson.name;
                                userToRegister.representingPerson.surname = request.body.representingPerson.surname;
                                userToRegister.company.name = request.body.company.name;
                                userToRegister.company.form = request.body.company.form;
                                userToRegister.company.taxId = request.body.company.taxId;
                                userToRegister.company.correspondenceAddress.city = request.body.company.correspondenceAddress.city;
                                userToRegister.company.correspondenceAddress.flatNumber = request.body.company.correspondenceAddress.flatNumber;
                                userToRegister.company.correspondenceAddress.houseNumber = request.body.company.correspondenceAddress.houseNumber;
                                userToRegister.company.correspondenceAddress.postalCode = request.body.company.correspondenceAddress.postalCode;
                                userToRegister.company.correspondenceAddress.street = request.body.company.correspondenceAddress.street;
                                userToRegister.company.registeredAddress.city = request.body.company.registeredAddress.city;
                                userToRegister.company.registeredAddress.flatNumber = request.body.company.registeredAddress.flatNumber;
                                userToRegister.company.registeredAddress.houseNumber = request.body.company.registeredAddress.houseNumber;
                                userToRegister.company.registeredAddress.postalCode = request.body.company.registeredAddress.postalCode;
                                userToRegister.company.registeredAddress.street = request.body.company.registeredAddress.street;
                                usersDao.save(userToRegister, function (isError) {
                                    if (isError) {
                                        consoleLogger.logError("registrationController", "Error while saving new user: " + isError);
                                        response.status(500).end();
                                    } else {
                                        emailSendService.sendVerificationEmail(userToRegister.email, userToRegister.emailVerification.code, function (error, info) {
                                            if (error) {
                                                consoleLogger.logError("registrationController", "Error while sending verification email: " + error + " : " + info);
                                                usersDao.removeBy({email: userToRegister.email}, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while removing user: " + isError);
                                                    }
                                                    response.status(500).end();
                                                });
                                            } else {
                                                consoleLogger.logInfo("registrationController", "Registered new user");
                                                usersDao.updateBy({'email': userToRegister.email}, {
                                                    emailVerification: {
                                                        isVerified: false,
                                                        code: userToRegister.emailVerification.code,
                                                        verificationSentTimestamp: moment()
                                                    }
                                                }, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while updating user: " + isError);
                                                        response.status(500).end();
                                                    } else {
                                                        response.status(201).end();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
        });
        app.post('/users/register/basic/borrower', requestLogger.logRequest, this.validateNewBorrowerSimpleInput, function (request, response) {
            usersDao.findSelectedBy("_id name", {email: request.body.email}, function (isError, results) {
                if (isError) {
                    consoleLogger.logError("registrationController", "Error while finding user by email: " + isError);
                    response.status(500).end();
                } else {
                    if (results != null && results.length > 0) {
                        consoleLogger.logWarn("registrationController", "User with email " + request.body.email + " already exists in DB");
                        response.status(409).end();
                    } else {
                        phoneVerificationsDao.findBy({
                            email: request.body.email,
                            phoneVerificationCode: request.body.phoneVerificationCode
                        }, function (isError, results) {
                            if (results == null || results.length == 0) {
                                consoleLogger.logWarn("registrationController", "No phone verification for email " + request.body.email + " and given verification code " + request.body.phoneVerificationCode + " at DataBase");
                                response.status(403).end();
                            } else {
                                var userToRegister = usersModel.createUser();
                                userToRegister.email = request.body.email;
                                userToRegister.password = usersModel.hashPassword(request.body.password);
                                userToRegister.role = usersModel.userRoles.BORROWER;
                                userToRegister.name = request.body.name;
                                userToRegister.surname = request.body.surname;
                                userToRegister.phone = results[0].phoneNumber;
                                userToRegister.timestampOfCreation = moment();
                                userToRegister.emailVerification.isVerified = false;
                                userToRegister.emailVerification.code = uuidGenerator.v1();
                                usersDao.save(userToRegister, function (isError, savedUser) {
                                    if (isError) {
                                        consoleLogger.logError("registrationController", "Error while saving new user: " + isError);
                                        response.status(500).end();
                                    } else {
                                        if (request.body.loanApplication != null && request.body.loanApplication != undefined) {
                                            var newAuction = auctionsModel.createAuction();
                                            newAuction.title = request.body.loanApplication.title;
                                            newAuction.description = request.body.loanApplication.description;
                                            newAuction.loanAmount = request.body.loanApplication.loanAmount;
                                            newAuction.loanTarget = request.body.loanApplication.loanTarget;
                                            newAuction.rateOfLoan = request.body.loanApplication.rateOfLoan;
                                            newAuction.timestampOfCreation = moment();
                                            newAuction.borrowerFk = savedUser._id.toString();
                                            newAuction.borrowerApplication.claimedAmount = request.body.loanApplication.loanAmount;
                                            newAuction.borrowerApplication.claimedPayoffPeriodInMonths = request.body.loanApplication.payoffPeriodInMonths;
                                            newAuction.borrowerApplication.claimedRateOfLoan = request.body.loanApplication.rateOfLoan;
                                            newAuction.borrowerApplication.timestamp = moment();
                                            auctionsDao.save(newAuction, function (isError) {
                                                if (isError) {
                                                    consoleLogger.logError("registrationController", "Cannot save new auction: " + isError);
                                                    response.status(500).end();
                                                } else {
                                                    consoleLogger.logInfo("registrationController", "Saved new auction for user with email: " + request.body.email);
                                                    response.status(201).end();
                                                }
                                            });
                                        }
                                        emailSendService.sendVerificationEmail(userToRegister.email, userToRegister.emailVerification.code, function (error, info) {
                                            if (error) {
                                                consoleLogger.logError("registrationController", "Error while sending verification email: " + error + " : " + info);
                                                usersDao.removeBy({email: userToRegister.email}, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while removing user: " + isError);
                                                    }
                                                    response.status(500).end();
                                                });
                                            } else {
                                                consoleLogger.logInfo("registrationController", "Registered new user");
                                                usersDao.updateBy({'email': userToRegister.email}, {
                                                    emailVerification: {
                                                        isVerified: false,
                                                        code: userToRegister.emailVerification.code,
                                                        verificationSentTimestamp: moment()
                                                    }
                                                }, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while updating user: " + isError);
                                                        response.status(500).end();
                                                    } else {
                                                        response.status(201).end();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
        });
        app.post('/users/register/full/borrower', requestLogger.logRequest, this.validateNewBorrowerFullInput, function (request, response) {
            usersDao.findSelectedBy("_id name", {email: request.body.email}, function (isError, results) {
                if (isError) {
                    consoleLogger.logError("registrationController", "Error while finding user by email: " + isError);
                    response.status(500).end();
                } else {
                    if (results != null && results.length > 0) {
                        consoleLogger.logWarn("registrationController", "User with email " + request.body.email + " already exists in DB");
                        response.status(409).end();
                    } else {
                        phoneVerificationsDao.findBy({
                            email: request.body.email,
                            phoneVerificationCode: request.body.phoneVerificationCode
                        }, function (isError, results) {
                            if (results == null || results.length == 0) {
                                consoleLogger.logWarn("registrationController", "No phone verification for email " + request.body.email + " and given verification code " + request.body.phoneVerificationCode + " at DataBase");
                                response.status(403).end();
                            } else {
                                var userToRegister = usersModel.createUser();
                                userToRegister.email = request.body.email;
                                userToRegister.password = usersModel.hashPassword(request.body.password);
                                userToRegister.role = usersModel.userRoles.BORROWER;
                                userToRegister.name = request.body.name;
                                userToRegister.surname = request.body.surname;
                                userToRegister.phone = results[0].phoneNumber;
                                userToRegister.timestampOfCreation = moment();
                                userToRegister.emailVerification.isVerified = false;
                                userToRegister.emailVerification.code = uuidGenerator.v1();
                                userToRegister.registeredAddress.city = request.body.registeredAddress.city;
                                userToRegister.registeredAddress.street = request.body.registeredAddress.street;
                                userToRegister.registeredAddress.postalCode = request.body.registeredAddress.postalCode;
                                userToRegister.registeredAddress.houseNumber = request.body.registeredAddress.houseNumber;
                                userToRegister.registeredAddress.flatNumber = request.body.registeredAddress.flatNumber;
                                userToRegister.correspondenceAddress.city = request.body.correspondenceAddress.city;
                                userToRegister.correspondenceAddress.street = request.body.correspondenceAddress.street;
                                userToRegister.correspondenceAddress.postalCode = request.body.correspondenceAddress.postalCode;
                                userToRegister.correspondenceAddress.houseNumber = request.body.correspondenceAddress.houseNumber;
                                userToRegister.correspondenceAddress.flatNumber = request.body.correspondenceAddress.flatNumber;
                                userToRegister.ratingParameters.age = request.body.ratingParameters.age;
                                userToRegister.ratingParameters.currentDebt = request.body.ratingParameters.currentDebt;
                                userToRegister.ratingParameters.employmentArea = request.body.ratingParameters.employmentArea;
                                userToRegister.ratingParameters.incomeSource = request.body.ratingParameters.incomeSource;
                                userToRegister.ratingParameters.maritalStatus = request.body.ratingParameters.maritalStatus;
                                userToRegister.ratingParameters.maritalStatusPeriodInMonths = request.body.ratingParameters.maritalStatusPeriodInMonths;
                                userToRegister.ratingParameters.monthlyAverageIncome = request.body.ratingParameters.monthlyAverageIncome;
                                userToRegister.ratingParameters.numberOfPeopleDependent = request.body.ratingParameters.numberOfPeopleDependent;
                                usersDao.save(userToRegister, function (isError, savedUser) {
                                    if (isError) {
                                        consoleLogger.logError("registrationController", "Error while saving new user: " + isError);
                                        response.status(500).end();
                                    } else {
                                        if (request.body.loanApplication != null && request.body.loanApplication != undefined) {
                                            var newAuction = auctionsModel.createAuction();
                                            newAuction.title = request.body.loanApplication.title;
                                            newAuction.description = request.body.loanApplication.description;
                                            newAuction.loanAmount = request.body.loanApplication.loanAmount;
                                            newAuction.loanTarget = request.body.loanApplication.loanTarget;
                                            newAuction.rateOfLoan = request.body.loanApplication.rateOfLoan;
                                            newAuction.timestampOfCreation = moment();
                                            newAuction.borrowerFk = savedUser._id.toString();
                                            newAuction.borrowerApplication.claimedAmount = request.body.loanApplication.loanAmount;
                                            newAuction.borrowerApplication.claimedPayoffPeriodInMonths = request.body.loanApplication.payoffPeriodInMonths;
                                            newAuction.borrowerApplication.claimedRateOfLoan = request.body.loanApplication.rateOfLoan;
                                            newAuction.borrowerApplication.timestamp = moment();
                                            auctionsDao.save(newAuction, function (isError) {
                                                if (isError) {
                                                    consoleLogger.logError("registrationController", "Cannot save new auction: " + isError);
                                                    response.status(500).end();
                                                } else {
                                                    consoleLogger.logInfo("registrationController", "Saved new auction for user with email: " + request.body.email);
                                                    response.status(201).end();
                                                }
                                            });
                                        }
                                        emailSendService.sendVerificationEmail(userToRegister.email, userToRegister.emailVerification.code, function (error, info) {
                                            if (error) {
                                                consoleLogger.logError("registrationController", "Error while sending verification email: " + error + " : " + info);
                                                usersDao.removeBy({email: userToRegister.email}, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while removing user: " + isError);
                                                    }
                                                    response.status(500).end();
                                                });
                                            } else {
                                                consoleLogger.logInfo("registrationController", "Registered new user");
                                                usersDao.updateBy({'email': userToRegister.email}, {
                                                    emailVerification: {
                                                        isVerified: false,
                                                        code: userToRegister.emailVerification.code,
                                                        verificationSentTimestamp: moment()
                                                    }
                                                }, function (isError) {
                                                    if (isError) {
                                                        consoleLogger.logError("registrationController", "Error while updating user: " + isError);
                                                        response.status(500).end();
                                                    } else {
                                                        response.status(201).end();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
        });
    }

};