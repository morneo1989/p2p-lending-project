var apiSecurityService = require("../../security/apiSecurityService");
var consoleLogger = require("../../../core/logger/consoleLogger");
var requestLogger = require("../requestsLogger");
var securityInMemoryStorage = require("../../security/securityInMemoryStorage");

module.exports = {

    validateUserLoginInput: function(request, response, next) {
        if(request.body.email == null || request.body.email == undefined) {
            consoleLogger.logError("loginController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else if(request.body.password == null || request.body.password == undefined) {
            consoleLogger.logError("loginController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },

    init: function(app, usersDao) {
        app.post('/users/login', requestLogger.logRequest, this.validateUserLoginInput, function(req, res, next) {
            usersDao.findBy({email: req.body.email}, function(isError, result) {
                if(isError) {
                    consoleLogger.logError("loginController", "Cannot access DB to get user: " + isError);
                    res.status(500).end();
                } else if(result == null || result.length == 0) {
                    consoleLogger.logInfo("loginController", "No user with given email: " + req.body.email);
                    res.status(404).end();
                } else {
                    if(result[0].emailVerification.isVerified) {
                        req.name = result[0].name;
                        req.surname = result[0].surname;
                        req.userType = result[0].role;
                        next();
                    } else {
                        res.json({emailVerified: false, name: result[0].name, surname: result[0].surname, userType: result[0].role}).end();
                    }

                }
            })

        }, apiSecurityService.authorizeUser, function(req, res) {
            consoleLogger.logDebug("loginController", "Authorized user: " + JSON.stringify(req.body));
            res.json({emailVerified: true, name: req.name, surname: req.surname, userType: req.userType}).end();
        });
        app.get('/users/login/validate', requestLogger.logRequest, apiSecurityService.authenticateRequest, function(req, res) {
            consoleLogger.logDebug("loginController", "User token still valid");
            res.status(200).end();
        });
    }
};