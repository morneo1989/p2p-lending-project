var apiSecurityService = require("../../security/apiSecurityService");
var usersModel = require("../../../db/model/main/users");
var consoleLogger = require("../../../core/logger/consoleLogger");
var requestLogger = require("../requestsLogger");
var uuidGenerator = require("node-uuid");
var emailSendService = require("../../../core/email/emailSendService");

module.exports = {

    validateUserPasswordResetInput: function (request, response, next) {
        if (request.body.oldPassword == null || request.body.oldPassword == undefined) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else if (request.body.newPassword == null || request.body.newPassword == undefined) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },

    validateUserPasswordRememberResetInput: function (request, response, next) {
        if (request.body.password == null || request.body.password == undefined) {
            consoleLogger.logError("registrationController", request.method + " - " + request.path + ": Invalid input for request");
            response.status(400).end();
        } else {
            next();
        }
    },

    init: function (app, usersDao) {
        app.post('/users/password/reset', requestLogger.logRequest, this.validateUserPasswordResetInput, apiSecurityService.authenticateRequest, function (request, response) {
            usersDao.findBy({email: request.email}, function (isError, result) {
                if (isError) {
                    consoleLogger.logError("manageController", "Cannot get user data from DB: " + isError);
                    response.status(500).end();
                } else if (result.length != 1) {
                    consoleLogger.logError("manageController", "More then one user with email: " + request.email);
                    response.status(500).end();
                } else if (result.length == 0) {
                    consoleLogger.logError("manageController", "No user with email: " + request.email);
                    response.status(500).end();
                } else {
                    var user = result[0];
                    if (usersModel.verifyPassword(request.body.oldPassword, user.password)) {
                        usersDao.updateBy({email: request.email}, {password: usersModel.hashPassword(request.body.newPassword)}, function (isError, numAffected) {
                            if (isError) {
                                consoleLogger.logError("manageController", "Cannot update password at DB: " + isError);
                                response.status(500).end();
                            } else if (numAffected.nModified != 1) {
                                consoleLogger.logError("manageController", "Cannot update user with email: " + request.email);
                                response.status(200).end();
                            } else {
                                consoleLogger.logError("manageController", "Updated password for user with email: " + request.email);
                                response.status(200).end();
                            }
                        });
                    } else {
                        consoleLogger.logError("manageController", "Invalid value of old password");
                        response.status(403).end();
                    }
                }
            });
        });
        app.get('/users/password/remember', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (request, response) {
            usersDao.findSelectedBy("email password passwordResetCode",
                {email: request.email}, function (isError, result) {
                    if (isError) {
                        consoleLogger.logError("manageController", "Error while finding user by email: " + isError);
                        response.status(500).end();
                    } else if (result == null || result.length == 0) {
                        consoleLogger.logError("manageController", "No user found by email:" + request.email);
                        response.status(404).end();
                    } else {
                        var registeredUser = result[0];
                        if (registeredUser.emailVerification.isVerified) {
                            consoleLogger.logDebug("manageController", "User's email already verified");
                            response.append('emailVerificationResponse', 1);
                            response.redirect('http://52.29.74.108:9080/user');
                        } else {
                            var resetCode = uuidGenerator.v1();
                            usersDao.updateBy({'email': request.email}, {
                                password: resetCode,
                                passwordResetCode: resetCode
                            }, function (isError) {
                                if (isError) {
                                    consoleLogger.logError("manageController", "Error while updating user: " + isError);
                                    response.status(500).end();
                                } else {
                                    emailSendService.sendPasswordResetEmail(request.email, resetCode, function (error, info) {
                                        if (error) {
                                            consoleLogger.logError("manageController", "Error while sending password reset email: " + error + " : " + info);
                                            response.status(500).end();
                                        } else {
                                            consoleLogger.logInfo("manageController", "Sent password reset email");
                                            response.status(200).end();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
        });
        app.get('/users/password/remember/:resetCode', requestLogger.logRequest, function (request, response) {
            var resetCode = request.params.resetCode;
            usersDao.findSelectedBy("email password passwordResetCode",
                {passwordResetCode: resetCode}, function (isError, result) {
                    if (isError) {
                        consoleLogger.logError("manageController", "Error while finding user by password reset code: " + isError);
                        response.redirect('http://52.29.74.108:9080/');
                    } else if (result == null || result.length == 0) {
                        consoleLogger.logError("manageController", "No user found by password reset code");
                        response.redirect('http://52.29.74.108:9080/');
                    } else {
                        response.redirect('http://52.29.74.108:9080/');
                    }
                });
        });
        app.post('/users/password', requestLogger.logRequest, this.validateUserPasswordRememberResetInput, apiSecurityService.authenticateRequest, function (request, response) {
            usersDao.findSelectedBy("email password passwordResetCode",
                {email: request.email}, function (isError, result) {
                    if (isError) {
                        consoleLogger.logError("manageController", "Error while finding user by email: " + isError);
                        response.status(500).end();
                    } else if (result == null || result.length == 0) {
                        consoleLogger.logError("manageController", "No user found by email");
                        response.status(404).end();
                    } else {
                        usersDao.updateBy({'email': request.email}, {
                            password: request.body.password,
                            passwordResetCode: null
                        }, function (isError) {
                            if (isError) {
                                consoleLogger.logError("manageController", "Error while updating user: " + isError);
                                response.status(500).end();
                            } else {
                                response.status(200).end();
                            }
                        });
                    }
                });
        });

        app.get('/users/user', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (request, response) {
            usersDao.findBy({email: request.email}, function (isError, result) {
                if (isError) {
                    consoleLogger.logError("manageController", "Error while finding user by email: " + isError);
                    response.status(500).end();
                } else if (result == null || result.length == 0) {
                    consoleLogger.logError("manageController", "No user found by email");
                    response.status(404).end();
                } else {
                    response.json(result[0]).end();
                }
            });
        });
        app.put('/users/investor', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (request, response) {

            var dataToUpdate = {};
            var checkIfValueNonNull = function (value) {
                return (value === null || value === undefined);
            };

            if(checkIfValueNonNull(request.body.company.name)) {
                dataToUpdate.company.name = request.body.company.name;
            }
            if(checkIfValueNonNull(request.body.company.form)) {
                dataToUpdate.company.name = request.body.company.form;
            }
            if(checkIfValueNonNull(request.body.company.taxId)) {
                dataToUpdate.company.name = request.body.company.taxId;
            }
            if(checkIfValueNonNull(request.body.company.correspondenceAddress.city)) {
                dataToUpdate.company.name = request.body.company.correspondenceAddress.city;
            }
            if(checkIfValueNonNull(request.body.company.correspondenceAddress.flatNumber)) {
                dataToUpdate.company.name = request.body.company.correspondenceAddress.flatNumber;
            }
            if(checkIfValueNonNull(request.body.company.correspondenceAddress.houseNumber)) {
                dataToUpdate.company.name = request.body.company.correspondenceAddress.houseNumber;
            }
            if(checkIfValueNonNull(request.body.company.correspondenceAddress.postalCode)) {
                dataToUpdate.company.name = request.body.company.correspondenceAddress.postalCode;
            }
            if(checkIfValueNonNull(request.body.company.correspondenceAddress.street)) {
                dataToUpdate.company.name = request.body.company.correspondenceAddress.street;
            }
            if(checkIfValueNonNull(request.body.company.registeredAddress.city)) {
                dataToUpdate.company.name = request.body.company.registeredAddress.city;
            }
            if(checkIfValueNonNull(request.body.company.registeredAddress.flatNumber)) {
                dataToUpdate.company.name = request.body.company.registeredAddress.flatNumber;
            }
            if(checkIfValueNonNull(request.body.company.registeredAddress.houseNumber)) {
                dataToUpdate.company.name = request.body.company.registeredAddress.houseNumber;
            }
            if(checkIfValueNonNull(request.body.company.registeredAddress.postalCode)) {
                dataToUpdate.company.name = request.body.company.registeredAddress.postalCode;
            }
            if(checkIfValueNonNull(request.body.company.registeredAddress.street)) {
                dataToUpdate.company.name = request.body.company.registeredAddress.street;
            }

            usersDao.updateBy({'email': request.email}, dataToUpdate, function (isError, numAffected) {
                if (isError) {
                    consoleLogger.logError("manageController", "Error while updating user: " + isError);
                    response.status(500).end();
                } else if(numAffected == 0) {
                    consoleLogger.logError("manageController", "No user updated by email: " + request.email);
                    response.status(404).end();
                } else {
                    response.status(200).end();
                }
            });
        });
        app.put('/users/borrower', requestLogger.logRequest, apiSecurityService.authenticateRequest, function (request, response) {

            var dataToUpdate = {};
            var checkIfValueNonNull = function (value) {
                return (value === null || value === undefined);
            };

            if(checkIfValueNonNull(request.body.registeredAddress.city)) {
                dataToUpdate.registeredAddress.city = request.body.registeredAddress.city;
            }
            if(checkIfValueNonNull(request.body.registeredAddress.street)) {
                dataToUpdate.registeredAddress.street = request.body.registeredAddress.street;
            }
            if(checkIfValueNonNull(request.body.registeredAddress.postalCode)) {
                dataToUpdate.registeredAddress.postalCode = request.body.registeredAddress.postalCode;
            }
            if(checkIfValueNonNull(request.body.registeredAddress.houseNumber)) {
                dataToUpdate.registeredAddress.houseNumber = request.body.registeredAddress.houseNumber;
            }
            if(checkIfValueNonNull(request.body.registeredAddress.flatNumber)) {
                dataToUpdate.registeredAddress.flatNumber = request.body.registeredAddress.flatNumber;
            }
            if(checkIfValueNonNull(request.body.correspondenceAddress.city)) {
                dataToUpdate.correspondenceAddress.city = request.body.correspondenceAddress.city;
            }
            if(checkIfValueNonNull(request.body.correspondenceAddress.street)) {
                dataToUpdate.correspondenceAddress.street = request.body.correspondenceAddress.street;
            }
            if(checkIfValueNonNull(request.body.correspondenceAddress.postalCode)) {
                dataToUpdate.correspondenceAddress.postalCode = request.body.correspondenceAddress.postalCode;
            }
            if(checkIfValueNonNull(request.body.correspondenceAddress.houseNumber)) {
                dataToUpdate.correspondenceAddress.houseNumber = request.body.correspondenceAddress.houseNumber;
            }
            if(checkIfValueNonNull(request.body.correspondenceAddress.flatNumber)) {
                dataToUpdate.correspondenceAddress.flatNumber = request.body.correspondenceAddress.flatNumber;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.age)) {
                dataToUpdate.ratingParameters.age = request.body.ratingParameters.age;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.currentDebt)) {
                dataToUpdate.ratingParameters.currentDebt = request.body.ratingParameters.currentDebt;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.employmentArea)) {
                dataToUpdate.ratingParameters.employmentArea = request.body.ratingParameters.employmentArea;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.incomeSource)) {
                dataToUpdate.ratingParameters.incomeSource = request.body.ratingParameters.incomeSource;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.maritalStatus)) {
                dataToUpdate.ratingParameters.maritalStatus = request.body.ratingParameters.maritalStatus;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.maritalStatusPeriodInMonths)) {
                dataToUpdate.ratingParameters.maritalStatusPeriodInMonths = request.body.ratingParameters.maritalStatusPeriodInMonths;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.monthlyAverageIncome)) {
                dataToUpdate.ratingParameters.monthlyAverageIncome = request.body.ratingParameters.monthlyAverageIncome;
            }
            if(checkIfValueNonNull(request.body.ratingParameters.numberOfPeopleDependent)) {
                dataToUpdate.ratingParameters.numberOfPeopleDependent = request.body.ratingParameters.numberOfPeopleDependent;
            }

            usersDao.updateBy({'email': request.email}, dataToUpdate, function (isError, numAffected) {
                if (isError) {
                    consoleLogger.logError("manageController", "Error while updating user: " + isError);
                    response.status(500).end();
                } else if(numAffected == 0) {
                    consoleLogger.logError("manageController", "No user updated by email: " + request.email);
                    response.status(404).end();
                } else {
                    response.status(200).end();
                }
            });
        });
    }
};