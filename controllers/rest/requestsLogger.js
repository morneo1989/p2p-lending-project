var consoleLogger = require("../../core/logger/consoleLogger");

module.exports = {
    logRequest: function(req, res, next) {
        consoleLogger.logDebug("requestsLogger", req.method + " made on path " + req.path + " with body: " + JSON.stringify(req.body));
        next();
    }
};