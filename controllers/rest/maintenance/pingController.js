var moment = require("moment");
var requestLogger = require("../requestsLogger");

module.exports = {
    init: function(app) {
        app.get('/maintenance/ping', requestLogger.logRequest, function(req, res) {
            res.status(200).send("Server alive with time: " + moment().format());
        });
    }
};