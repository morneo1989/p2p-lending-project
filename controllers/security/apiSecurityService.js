var consoleLogger = require("../../core/logger/consoleLogger");
var moment = require("moment");
var uuidGenerator = require("node-uuid");
var jwtSimple = require("jwt-simple");
var usersModel = require("../../db/model/main/users");
var usersDao = require("../../db/dao/main/usersDAO");
var securityInMemoryStorage = require("./securityInMemoryStorage");

module.exports = {

    authorizeUser: function(request, response, next) {
        usersDao.findBy({email: request.body.email}, function(isError, result) {
            if(isError) {
                consoleLogger.logError("apiSecurityService", "Error while getting user from Database: " + isError);
                response.status(500).end();
            } else if(result.length > 1) {
                consoleLogger.logError("apiSecurityService", "More then one user with email: " + request.body.email + " at Database");
                response.status(500).end();
            } else if(result.length == 0) {
                consoleLogger.logError("apiSecurityService", "No user with email: " + request.body.email + " at Database");
                response.status(404).end();
            } else {
                var user = result[0];
                if(!usersModel.verifyPassword(request.body.password, user.password)) {
                    consoleLogger.logError("apiSecurityService", "User password invalid");
                    response.status(403).end();
                }
                var payloadToEncode = {email: request.body.email, timestamp: moment().unix()};
                var secretKeyToEncode = uuidGenerator.v1().substring(0,20);
                var newToken = jwtSimple.encode(payloadToEncode, secretKeyToEncode);
                var splittedToken = newToken.split('.');
                newToken = secretKeyToEncode + "." + splittedToken[1] + "." + splittedToken[2];
                response.append('x-auth', newToken);
                next();
            }
        });
    },

    authenticateRequest: function(request, response, next) {
        if(request.get('x-auth') == null || request.get('x-auth') == undefined) {
            consoleLogger.logWarn("apiSecurityService", "No authentication data given");
            response.status(403).end();
        } else {
            var retrievedToken = request.get('x-auth');
            console.log("retrieved token: " + retrievedToken);
            var splittedToken = retrievedToken.split('.');
            if(splittedToken.length != 3) {
                consoleLogger.logError("apiSecurityService", "Invalid format of authentication token");
                response.status(403).end();
            } else {
                var secretKey = splittedToken[0];
                var header = new Buffer('{"typ":"JWT","alg":"HS256"}').toString('base64');
                retrievedToken = header + "." + splittedToken[1] + "." + splittedToken[2];
                var decoded = jwtSimple.decode(retrievedToken, secretKey);
                request.email = decoded.email;
                var currentTimestampInMillis = moment().unix();
                if(currentTimestampInMillis - decoded.timestamp >= 30*60) {
                    consoleLogger.logInfo("apiSecurityService", "User login timeout");
                    response.status(403).end()
                } else {
                    consoleLogger.logInfo("apiSecurityService", JSON.stringify(decoded));
                    consoleLogger.logInfo("apiSecurityService", "Time: " + (currentTimestampInMillis - decoded.timestamp) + " seconds");
                    request.body.email = decoded.email;
                    next();
                }
            }
        }
    }

};