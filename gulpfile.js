var gulp = require('gulp');
var mocha = require('gulp-mocha');
var consoleLogger = require("./core/logger/consoleLogger");
var sftp = require("gulp-sftp");
var changed = require("gulp-changed");

gulp.task('default', function () {
    gulp.src('test/**/*.js', {read: false})
        .pipe(mocha({reporter: 'list'}));

});

gulp.task('deploy-via-sftp', function () {
    gulp.src(['../p2p-lending-project/**', '!../p2p-lending-project/node_modules/**', '!../p2p-lending-project/gulpfile.js', '!../p2p-lending-project/test/**',
        '!../p2p-lending-project/.gitignore', '!../p2p-lending-project/dist/**'])
        .pipe(changed('./dist/'))
        .pipe(gulp.dest('./dist/'))
        .pipe(sftp({
            host: 'ec2-52-29-74-108.eu-central-1.compute.amazonaws.com',
            user: 'ec2-user',
            key: '/home/maciek/.ssh/p2pLendingPlatformSSHKey.pem',
            remotePath: '/pospo_www/development'
        }))
});