var express = require("express");
var consoleLogger = require("./core/logger/consoleLogger");
var fileLogger = require("./core/logger/fileLogger");
var databaseLogger = require("./core/logger/databaseLogger");
var bodyParser = require("body-parser");
var usersModel = require("./db/model/main/users");
var cors = require('cors');

var expressMainApp = express();

consoleLogger.setEnabledLogLevel("DEBUG");

var corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": true,
    "optionsSuccessStatus": 204,
    "exposedHeaders": 'x-auth'
}

expressMainApp.use(cors(corsOptions));
expressMainApp.use(bodyParser.json());

var path = require('path');

/* Configuration for static endpoint displayed until system POSPO will be ready */
/*var endPointApp = express();
 var path = require('path');
 endPointApp.use('/assets', express.static('/home/ec2-user/pospo_project/development/public/static_endpoint/assets'));
 endPointApp.get("/", function (req, res) {
 res.sendFile(path.join('/home/ec2-user/pospo_project/development/public/static_endpoint/index.html'));
 });
 endPointApp.listen(8080);*/

/* Configuration of MongoDB DAO layer */

var mongoose = require("mongoose");
var usersDao = require("./db/dao/main/usersDAO");
var auctionsDao = require("./db/dao/main/auctionsDAO");
var investmentsDao = require("./db/dao/main/investorSharesDAO");
var legalRegulationsDao = require("./db/dao/administration/moderationContentDAO");
var systemStatisticsDao = require("./db/dao/administration/systemStatisticsDAO");
var phoneVerificationsDao = require("./db/dao/main/phonesVerificationDAO");

var saveSampleInvestor = function () {
    consoleLogger.logInfo('server.js', 'Saving sample investor user');
    var investorUser = usersModel.createUser();
    investorUser.email = "investor@pospo.pl";
    investorUser.password = usersModel.hashPassword("investor");
    investorUser.name = "Fundusze";
    investorUser.surname = "Europejskie";
    investorUser.emailVerification.isVerified = true;
    investorUser.role = usersModel.userRoles.INVESTOR;
    usersDao.save(investorUser, function (isError) {
        if (isError) {
            consoleLogger.logError("server.js", "Error occurred during investor user save: " + isError);
        } else {
            consoleLogger.logInfo("server.js", "Sample Investor User saved");
        }
    })
};

var saveSampleBorrower = function (saveCallback) {
    consoleLogger.logInfo('server.js', 'Saving sample borrower user');
    var sampleBorrower = usersModel.createUser();
    sampleBorrower.email = "borrower@pospo.pl";
    sampleBorrower.password = usersModel.hashPassword("borrower");
    sampleBorrower.name = "Donald";
    sampleBorrower.surname = "Tusk";
    sampleBorrower.emailVerification.isVerified = true;
    sampleBorrower.role = usersModel.userRoles.BORROWER;
    usersDao.save(sampleBorrower, function (isError) {
        if (isError) {
            consoleLogger.logError("server.js", "Error occurred during admin user save: " + isError);
            saveCallback(true);
        } else {
            consoleLogger.logInfo("server.js", "Sample Borrower User saved");
            saveCallback(false);
        }
    })
};

var saveAdminUser = function (saveCallback) {
    consoleLogger.logInfo('server.js', 'Saving admin user');
    var adminUser = usersModel.createUser();
    adminUser.email = "admin@pospo.pl";
    adminUser.password = usersModel.hashPassword("admin");
    adminUser.name = "Angela";
    adminUser.surname = "Merkel";
    adminUser.emailVerification.isVerified = true;
    adminUser.role = usersModel.userRoles.ADMIN;
    usersDao.save(adminUser, function (isError) {
        if (isError) {
            consoleLogger.logError("server.js", "Error occurred during admin user save: " + isError);
            saveCallback(true);
        } else {
            consoleLogger.logInfo("server.js", "Admin user saved");
            saveCallback(false);
        }
    })
};

mongoose.connect('mongodb://localhost:27017/p2pLendingDB', function (err) {
    if (err) {
        console.log('Error occurred on Mongo DB' + err);
        throw new Error('Error occurred on Mongo DB' + err + " for url: mongodb://localhost:27017/p2pLendingDB")
    } else {
        consoleLogger.logInfo('server.js', 'Connected to Mongo DB');
        console.log('Connected to Mongo DB');
        usersDao.init(mongoose);
        auctionsDao.init(mongoose);
        investmentsDao.init(mongoose);
        legalRegulationsDao.init(mongoose);
        systemStatisticsDao.init(mongoose);
        phoneVerificationsDao.init(mongoose);

        /* Initialization of DB data */
        usersDao.findBy({email: 'admin@pospo.pl'}, function (isError, results) {
            if (isError) {
                consoleLogger.logError("server.js", 'Error while getting admin user from DB: ' + isError);
            } else {
                if (results == null || results.length == 0) {
                    saveAdminUser(function (isError) {
                        if (!isError) {
                            usersDao.findBy({email: 'borrower@pospo.pl'}, function (isError, results) {
                                if (isError) {
                                    consoleLogger.logError("server.js", 'Error while getting borrower user from DB: ' + isError);
                                } else {
                                    if (results == null || results.length == 0) {
                                        saveSampleBorrower(function (isError) {
                                            if (!isError) {
                                                usersDao.findBy({email: 'investor@pospo.pl'}, function (isError, results) {
                                                    if (!isError) {
                                                        saveSampleInvestor();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }
});


/* Initialization of REST interface controllers */
var pingController = require("./controllers/rest/maintenance/pingController");
pingController.init(expressMainApp);
var loginController = require("./controllers/rest/users/loginController");
loginController.init(expressMainApp, usersDao);
var registrationController = require("./controllers/rest/users/registrationController");
registrationController.init(expressMainApp, usersDao, auctionsDao, phoneVerificationsDao);
var usersManageController = require("./controllers/rest/users/manageController");
usersManageController.init(expressMainApp, usersDao);
var investmentsController = require("./controllers/rest/investments/investmentsController");
investmentsController.init(expressMainApp, usersDao, investmentsDao);
var auctionsController = require("./controllers/rest/auctions/auctionsController");
auctionsController.init(expressMainApp, usersDao, auctionsDao, investmentsDao);
var legalRegulationsController = require("./controllers/rest/administration/moderationController");
legalRegulationsController.init(expressMainApp, legalRegulationsDao);
var statisticsController = require("./controllers/rest/statisticsController");
statisticsController.init(expressMainApp, systemStatisticsDao);

/* Initialization of core modules */


/* Initialize configuration for POSPO system from database */

expressMainApp.use(express.static('/pospo_www/development/public'));
expressMainApp.get("/pospo/", function (req, res) {
    res.sendFile(path.join('/pospo_www/development/public/index.html'));
});
expressMainApp.get("/pospo/*", function (req, res) {
    res.sendFile(path.join('/pospo_www/development/public/index.html'));
});
expressMainApp.get("/pospo", function (req, res) {
    res.sendFile(path.join('/pospo_www/development/public/index.html'));
});

expressMainApp.listen(9080);




